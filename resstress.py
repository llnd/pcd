"Sets up a decaying elliptical residual stress around the inclusions."

import numpy as np

def FindR(x,y,theta,e):
    "Find the value of the 'radius' of the ellipse (with e as the y denominator) outside the inclusion"
    r = np.sqrt((x*np.cos(theta) + y*np.sin(theta))**2 + ((x*np.sin(theta) - y*np.cos(theta))/e)**2)
    r[r < 1] = 0
    return r

def StressDecay(R,g,ampl):
    "Equation describing how quickly the stress decays with the 'radius'"
    sigma = ampl * np.exp(-R*g)
    return sigma

def RStress(Xq,Yq,thetar,e,l1,g,ampl):
    "Generates a decaying residual stress around ellipses."
    # Find 'radius' of ellipse outside the inclusion
    R = FindR(Xq, Yq, thetar[:, np.newaxis, np.newaxis], e[:, np.newaxis, np.newaxis])
    offR = R-l1[:, np.newaxis, np.newaxis]      # radius offset so stress = ampl around the perimeter

    # Create elliptical residual stresses
    rstressq = StressDecay(offR,g,ampl)         # individual residual stresses
    rstress = np.sum(rstressq, axis=0)          # combined residual stresses
    maskq = np.ma.masked_where(R == 0,rstressq) # individual masked ellipses
    mask = np.sum(maskq.mask, axis=0)           # the mask of all ellipses
    rStress = np.sum(maskq, axis=0)             # masked combined residual stresses

    return rStress, mask
