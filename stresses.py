"Computes the global stresses from the complex potentials."

import numpy as np

def dz(potential, hx, hy):
    """
    Calculate the derivative wrt z where z = x+iy, with different step sizes.
    """
    dy, dx = np.gradient(potential,hy,hx)
    dz = dx - 1j*dy
    return dz

def dPot(EllCoords, Psi, Phi, hx, hy, minuseith, wprime, Ntot):
    """
    Differentiates the potentials in dz/dxi, depending on the value of EllCoords.
    """
    dPsi, dPhi, d2Phi = [], [], []

    if EllCoords == False:
        for i in range(Ntot):
            dpsi = dz(Psi[i], hx, hy) / minuseith[i]
            dphi = dz(Phi[i], hx, hy) / minuseith[i]
            ddphi = dz(dphi, hx, hy) / minuseith[i]
            dPsi.append(dpsi)
            dPhi.append(dphi)
            d2Phi.append(ddphi)
        dPsi = np.asarray(dPsi)
        dPhi = np.asarray(dPhi)
        d2Phi = np.asarray(d2Phi)

    else:
        for i in range(Ntot):
            dpsi = dz(Psi[i], hx[i], hy[i]) / wprime[i]
            dphi = dz(Phi[i], hx[i], hy[i]) / wprime[i]
            ddphi = dz(dphi, hx[i], hy[i]) / wprime[i]
            dPsi.append(dpsi)
            dPhi.append(dphi)
            d2Phi.append(ddphi)
        dPsi = np.asarray(dPsi)
        dPhi = np.asarray(dPhi)
        d2Phi = np.asarray(d2Phi)

    return dPsi, dPhi, d2Phi

def Stresses(zq, minuseith, dPsi, dPhi, d2Phi, G0, thetar):
    """
    Calculates the stresses from the derivatives of the potentials and rotates
    them into the global coordinate system.
    """

    yq = zq * minuseith[:, np.newaxis, np.newaxis]
    sum = 4 * G0 * (dPhi + dPhi.conjugate()).real
    rhs = 4 * G0 * ((yq.conjugate()-yq) * d2Phi - dPhi + dPsi)
    diff = rhs.real
    s12 = 0.5 * rhs.imag
    s22 = (0.5 * (sum+diff))
    s11 = (0.5 * (sum-diff))

    sigma11, sigma12, sigma22 = Rotate(s11,s22,s12,thetar)

    return sigma11, sigma12, sigma22

def Rotate(s11, s22, s12, thetar):
    """
    Rotates the stress fields from the local yq to zq coordinate systems for each inclusion.
    """
    # thetar = -thetar
    cos = np.cos(thetar)
    cos2 = np.cos(thetar*2)
    cos = cos[:, np.newaxis, np.newaxis]
    cos2 = cos2[:, np.newaxis, np.newaxis]
    sin = np.sin(thetar)
    sin2 = np.sin(thetar*2)
    sin = sin[:, np.newaxis, np.newaxis]
    sin2 = sin2[:, np.newaxis, np.newaxis]

    sigma11 = s11 * cos**2 - 2*s12*cos*sin + s22 * sin**2
    sigma12 = s12 * cos2 + (s11 - s22)*cos*sin
    sigma22 = s22 * cos**2 + s11*sin**2 + s12 * sin2

    return sigma11, sigma12, sigma22
