""" This file currently includes the physical constants, the inital far field stress,
and the functions to visualise the ellipses and set up local coordinate systems. """

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse

def PhysConsts(Ntot):
    """
    Defines physical constants - poisson's ratio (nu), shear modulus (G),
    and kappa (k).

    Kappa is a parameter used in complex variable elasticity, = 3-4*nu for
    plane strain, and 3-4*nu/1+nu for plane stress. Matrix variables are denoted
    with (0), inclusion variables with (1).

    For cobalt, nu = 0.31, G = 75 GPa;
    For diamond, nu = 0.07, G = 478 GPa;
    For a crack or hole, nu = G = 0.

    Parameters
    ----------
    Ntot : int
        The total number of inclusions in the matrix.

    Returns
    -------
    nu0 : float
        The poisson's ratio of the matrix
    G0 : float
        The shear modulus of the matrix (in GPa)
    k0 : float
        Kappa for the matrix in plane strain
    nu1 : array
        The poisson's ratio for each of the N inclusions
    G1 : array
        The shear modulus for each of the N inclusions (in GPa)
    k1 : array
        Kappa for each of the N inclusions in plane strain

    """

    # Matrix (Diamond)
    nu0 = 0.07
    G0 = 570
    k0 = 3-4*nu0

    # Inclusions (Cracks)
    nu1 = np.zeros(Ntot)
    G1 = np.zeros(Ntot)
    k1 = 3-4*nu1

    return nu0, G0, k0, nu1, G1, k1

def FarFieldStress(ang):
    """
    Defines the 2D stress state from an imposed far field load.

    Returns
    -------
    S11 : float
        The stress in the x-direction
    S22 : float
        The stress in the y-direction
    S12 : float
        The stress in the xy-direction

    """

    s11 = 0
    s22 = 1
    s12 = 0

    angle = ang/180 * np.pi

    S11 = s11 * np.cos(angle)**2 - 2*s12*np.cos(angle)*np.sin(angle) + s22*np.sin(angle)**2
    S12 = s12 * np.cos(2*angle) + (s11 - s22)*np.cos(angle)*np.sin(angle)
    S22 = s22 * np.cos(angle)**2 + s11*np.sin(angle)**2 + s12*np.sin(2*angle)

    return S11, S22, S12

def StartPicture(limit, zcenter, l1, l2, theta, Ntot):
    """
    Initialises a plot of the initial geometry/ setup of inclusions,
    which have been specified in the main function.

    Parameters
    -------
    limit : float
        The x and y limit buffers of the plot
    zcenter : array
        The locations of the center of the inclusions
    l1 : array
        Major axis lengths of each inclusion
    l2 : array
        Minor axis lengths of each inclusion
    Ntot : int
        The total number of inclusions in the matrix.

    """
    xmin, xmax = np.amin(zcenter.real) - limit, np.amax(zcenter.real) + limit
    ymin, ymax = np.amin(zcenter.imag) - limit, np.amax(zcenter.imag) + limit
    fig, ax = plt.subplots()
    plt.ylim(ymin, ymax)
    plt.xlim(xmin, xmax)
    for i in range(Ntot):
        ell = Ellipse((zcenter.real[i],zcenter.imag[i]),2*l1[i],2*l2[i], theta[i], linewidth=2, fill=False)
        ax.add_patch(ell)
    plt.show()

def MakeXi(zcenter, limit, spacing, d, z):
    """
    Constructing individual arrays to calculate for each inclusion in z-space, so
    that the stresses can be summed directly.

    This creates a picture ranging from 'limit' to the left of the left inclusion,
    to 'limit' from the right of the right inclusion.

    Parameters
    -------
    zcenter: array
        The locations of the center of the inclusions
    limit : float
        The distance from the center of the edgemost ellipses, for finding the x and y limits
    spacing : integer
        The number of points in each direction (hence the fineness of the mesh to be constructed)
    d : array
        The inter-foci distance of the respective inclusions
    z : function
        z = x + iy

    Returns
    -------
    x,y : arrays
        The limits for the overall picture with zero centered between the inclusions
    X,Y : arrays
        The mesh for the overall picture with zero centered between the inclusions
    Xq,Yq : arrays
        The mesh for the overall picture with zero centered on the Xqth inclusion (q nested arrays)
    hx,hy : float
        The x/y stepsizes to accurately numerically differentiate for the created mesh
    zq : array
        Nested array containing the z mesh over which calculations are done for each inclusion
    Xi : array
        Nested array containing the mapped mesh (from the z-values) for each inclusion
    """

    xmin, xmax = np.amin(zcenter.real) - limit, np.amax(zcenter.real) + limit
    ymin, ymax = np.amin(zcenter.imag) - limit, np.amax(zcenter.imag) + limit

    # global grid for plotting
    x = np.linspace(xmin, xmax, spacing)
    y = np.linspace(ymin, ymax, spacing)
    X, Y = np.meshgrid(x,y)

    hx = 2 * (xmax - xmin) / spacing
    hy = 2 * (ymax - ymin) / spacing

    zq = []
    Xi = []
    Xq, Yq = [], []
    # local coordinate systems
    for i in range(len(d)):
        xleft = zcenter.real[i] - xmin
        xright = xmax - zcenter.real[i]
        ybot = zcenter.imag[i] - ymin
        ytop = ymax - zcenter.imag[i]
        xq = np.linspace(-xleft, xright, spacing)
        yq = np.linspace(-ybot, ytop, spacing)
        Xqi, Yqi = np.meshgrid(xq, yq)
        zqi = z(Xqi, Yqi)
        xiq = np.arccosh(zqi/d[i])
        zq.append(zqi)
        Xi.append(xiq)
        Xq.append(Xqi)
        Yq.append(Yqi)
    zq = np.asarray(zq)
    Xi = np.asarray(Xi)
    Xq = np.asarray(Xq)
    Yq = np.asarray(Yq)

    return x, y, X, Y, Xq, Yq, hx, hy, zq, Xi

def MakeXiE(z, limx, limy, spacing, Z0, d):
    """
    Set up the grid in elliptical coordinate space.

    Parameters
    -------
    z : function
        z = x + iy
    limx : integer
        Zeta limit in multiples of Z0; (spacing-1) must be divisible by limx to index Z0
    limy : float
        Eta limit (= pi, so that it runs from -pi to pi)
    spacing : integer
        The number of points in each direction (hence the fineness of the mesh to be constructed)
    Z0 : float
        The value of zeta which defines the ellipse
    d : array
        The inter-foci distance of the respective inclusions


    Returns
    -------
    Xi : array
        Nested array containing the zeta-eta mesh for each inclusion
    hx,hy : float
        The zeta/eta stepsizes to accurately numerically differentiate for the created mesh
    zq : array
        Nested array containing the cartesian coordinates that correspond to each zeta/eta value in Xi
    Z0_index : integer
        The index that gives the Z0 value in the array xi_zeta
    xi_eta, xi_zeta : arrays
        The limits for the overall picture with zero centered between the inclusions
    """
    
    Xi, hx, hy = [], [], []
    for q in range(len(d)):
        xi_zeta = np.linspace(0,limx*Z0[q],spacing)
        Z0_index = int((spacing-1)/limx)
        print(xi_zeta[Z0_index])  # see comment on limx
        xi_eta = np.linspace(0,2*limy,spacing)
        xiX, xiY = np.meshgrid(xi_zeta,xi_eta)
        Xi.append(z(xiX,xiY))
        hx.append(2*2*(0.5*limx*Z0[q])/spacing)
        hy.append(2*2*(limy)/spacing)
    Xi = np.asarray(Xi)
    hx = np.asarray(hx)
    hy = np.asarray(hy)
    zq = d[:,np.newaxis, np.newaxis] * np.cosh(Xi)

    return Xi, hx, hy, zq, Z0_index, xi_eta, xi_zeta
