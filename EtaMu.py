"Calculates the expansion coefficients Eta and Mu."

import numpy as np
from scipy.special import factorial

def FindMnm(m, n, k, l, dpdq):
    """
    Finds Mnml for each m and n, summed for each (m,n) up to a given value of l.

    Returns an array:
    first row: m = 1, n = 1,2,3...
    second row: m = 2, n = 1,2,3...
    """
    Mnm = []
    for j in range(len(m)):
        Mnm_n = []
        for i in range(len(n)):
            mnm = dpdq**(2*k)/ (factorial(k) * factorial(l-k) * factorial(k+n[i]) * factorial(m[j]+l-k))
            Mnm_samem = np.sum(mnm)
            Mnm_n.append(Mnm_samem)
        Mnm.append(Mnm_n)
    return Mnm

def FindMnml(m,n,l,dpdq):
    """
    Finds Mnml for the entire range of m, n, l.

    Inputs:
    m : array of values of m to be evaluated
    n : array of values of n to be evaluated
    l : array of values of l to be evaluated
    dpdq : scalar, dp/dq

    Outputs:
    Mnml : nested array; Mnml[i] gives l = i; Mnml[i][j][k] has indices given by l = i, m = j, n = k

    """
    Mnml = []
    for i in range(len(l)):
        k = np.arange(0, l[i]+1, 1)
        Mnm = FindMnm(m,n,k,l[i],dpdq)
        Mnml.append(Mnm)
    return Mnml

def EtaFarPQ(m,n,l,Mnml,dp,dq,zpq):
    """
    Calculate the expansion coefficients for distant inclusions.

    Returns an array where the rows give the m index and the columns are the n index,
    so e.g. eta_pq_far[0] is m = 1.
    """
    eta_pq_far = []
    for i in range(len(m)):
        m_value = m[i]
        for w in range(len(n)):
            n_value = n[w]
            "to sum over l for each mn"
            nm = Mnml[:,i,w] # value of Mnml for a fixed mn, for varying l : length of nm is length of l)
            sum_mn = (dq**(2*l+m_value) * nm * factorial(n_value + m_value + 2*l -1)
                / (2*zpq)**(n_value + m_value + 2*l)) * (-1)**(m_value) * n_value * dp**(n_value)
            sum = np.sum(sum_mn)
            eta_pq_far.append(sum)
    eta_pq_far= np.asarray(eta_pq_far).reshape(len(m),len(n))
    return eta_pq_far

def MuFarPQ(m,n,l,Mnml,dp,dq,zpq):
    """
    Calculate the expansion coefficients for distant inclusions.

    Returns an array where the rows give the m index and the columns are the n index,
    so e.g. eta_pq_far[0] is m = 1.
    """
    mu_pq_far = []
    for i in range(len(m)):
        m_value = m[i]
        for w in range(len(n)):
            n_value = n[w]
            "to sum over l for each mn"
            nm = Mnml[:,i,w] # value of Mnml for a fixed mn, for varying l : length of nm is length of l)
            sum_mn = (dq**(2*l+m_value) * nm * factorial(n_value + m_value + 2*l)
                / (2*zpq)**(n_value + m_value + 2*l + 1)) * (-1)**(m_value) * (-2) * n_value * dp**(n_value)
            sum = np.sum(sum_mn)
            mu_pq_far.append(sum)
    mu_pq_far= np.asarray(mu_pq_far).reshape(len(m),len(n))
    return mu_pq_far

def EtaNearPQ(m,n,l,j,Mnml,dp,dq,dpq,vpq):
    """
    Calculate the expansion coefficients for near inclusions.

    Returns an array where the rows give the m index and the columns are the n index,
    so e.g. eta_pq_far[0] is m = 1.
    """
    eta_pq_near = []
    for i in range(len(m)):
        m_value = m[i]
        for w in range(len(n)):
            n_value = n[w]
            "to sum over l for each mn"
            # print("m is ",m_value, "n is ",n_value)
            sumj = []
            for v in range(len(j)):
                j_value = j[v]
                nm = Mnml[:j_value+1,i,w] # value of Mnml for a fixed mn, for varying l, up to l=j
                trunc_l = l[:j_value+1] # truncating l so that the sum is only evaluated up to l=j
                # print(factorial(m_value + n_value + trunc_l + j_value -1))
                sum_l = ((-1)**(j_value-trunc_l)
                        / factorial(j_value-trunc_l)
                        * (dq/dpq)**(2*trunc_l + m_value)
                        * nm
                        * factorial(m_value + n_value + trunc_l + j_value -1)
                        * vpq**(-(n_value + m_value + 2*j_value))
                        * n_value * (dp/dpq)**n_value
                        * (-1)**m_value)
                sum_j = np.sum(sum_l)       # this sums over l for each j
                # print(j_value, sum_l, sum_j)
                sumj.append(sum_j)
            sum = np.sum(sumj)         # this sums over j for each m/n
            # print(sum)
            eta_pq_near.append(sum)
    eta_pq_near= np.asarray(eta_pq_near).reshape(len(m),len(n))
    return eta_pq_near

def MuNearPQ(m,n,l,j,Mnml,dp,dq,dpq,vpq,vpqprime):
    """
    Calculate the expansion coefficients for near inclusions.

    Returns an array where the rows give the m index and the columns are the n index,
    so e.g. eta_pq_far[0] is m = 1.
    """
    mu_pq_near = []
    for i in range(len(m)):
        m_value = m[i]
        for w in range(len(n)):
            n_value = n[w]
            "to sum over l for each mn"
            # print("m is ",m_value, "n is ",n_value)
            sumj = []
            for v in range(len(j)):
                j_value = j[v]
                nm = Mnml[:j_value+1,i,w] # value of Mnml for a fixed mn, for varying l, up to l=j
                trunc_l = l[:j_value+1] # truncating l so that the sum is only evaluated up to l=j
                # print(factorial(m_value + n_value + trunc_l + j_value -1))
                sum_l = ((-1)**(j_value-trunc_l)
                        / factorial(j_value-trunc_l)
                        * (dq/dpq)**(2*trunc_l + m_value)
                        * vpqprime
                        * nm
                        * factorial(m_value + n_value + trunc_l + j_value -1)
                        * (n_value + m_value + 2*j_value) * vpq**(-(n_value + m_value + 2*j_value + 1))
                        * (-n_value) * (dp/dpq)**n_value
                        * (-1)**m_value)
                sum_j = np.sum(sum_l)       # this sums over l for each j
                # print(j_value, sum_l, sum_j)
                sumj.append(sum_j)
            sum = np.sum(sumj)         # this sums over j for each m/n
            # print(sum)
            mu_pq_near.append(sum)
    mu_pq_near= np.asarray(mu_pq_near).reshape(len(m),len(n))
    return mu_pq_near

def ExpansionCoeffs(near,m,n,l,j,zpq,dpq,dpdq,vpq,vpqprime,pq,Ntot,d):
    """
    Finds Eta and Mu for each pair, according to the order of pairs in pq.
    """
    Mnml_pq = []
    for pair in range(len(pq)):
        Mnml_pq.append(FindMnml(m,n,l,dpdq[pair]))
    Mnml_pq = np.asarray(Mnml_pq)
    # (have not yet written the integral forms/ separated by near/far conditions, bc i want to move on first)
    eta, mu = [], []
    for i, pair in enumerate(pq):
        dp, dq = d[pair[0]], d[pair[1]]
        if near == False:
            eta.append(EtaFarPQ(m,n,l,Mnml_pq[i],dp,dq,zpq[i]))
            mu.append(MuFarPQ(m,n,l,Mnml_pq[i],dp,dq,zpq[i]))
        else:
            eta.append(EtaNearPQ(m,n,l,j,Mnml_pq[i],dp,dq,dpq[i],vpq[i]))
            mu.append(MuNearPQ(m,n,l,j,Mnml_pq[i],dp,dq,dpq[i],vpq[i],vpqprime[i]))
    eta = np.asarray(eta)
    mu = np.asarray(mu)

    return eta, mu
