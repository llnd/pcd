import numpy as np

limit = 3
spacing = 501
Ntot = 3
l1 = np.array([1,1,1])
l2 = np.array([0.5,0.2,0.3])
d = np.sqrt(l1*l1 - l2*l2)

zcenter = np.array([-2+0j,2+0j,4+1j])
xmin, xmax = np.amin(zcenter.real) - limit, np.amax(zcenter.real) + limit
ymin, ymax = np.amin(zcenter.imag) - limit, np.amax(zcenter.imag) + limit


def z(x,y):
    return x + 1j * y

# global grid for plotting
x = np.linspace(xmin, xmax, spacing)
y = np.linspace(ymin, ymax, spacing)
X, Y = np.meshgrid(x,y)

hx = 2 * (xmax - xmin) / spacing
hy = 2 * (ymax - ymin) / spacing

zq = []
Xi = []
Xq, Yq = [], []
# local coordinate systems
for i in range(Ntot):
    xleft = zcenter.real[i] - xmin
    xright = xmax - zcenter.real[i]
    ybot = zcenter.imag[i] - ymin
    ytop = ymax - zcenter.imag[i]
    x = np.linspace(-xleft, xright, spacing)
    y = np.linspace(-ybot, ytop, spacing)
    Xqi, Yqi = np.meshgrid(x, y)
    zqi = z(Xqi, Yqi)
    xiq = np.arccosh(zqi/d[i])
    zq.append(zqi)
    Xi.append(xiq)
    Xq.append(Xqi)
    Yq.append(Yqi)
zq = np.asarray(zq)
Xi = np.asarray(Xi)
Xq = np.asarray(Xq)
Yq = np.asarray(Yq)

print(zq)
