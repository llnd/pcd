import numpy as np
import matplotlib.pyplot as plt

l1 = 1
l2 = 0.4
e = l2/l1
ang = 15
theta = ang / 180 * np.pi
limit = 3
spacing = 100
x = np.linspace(-limit,limit,spacing)
y = np.linspace(-limit,limit,spacing)
X, Y = np.meshgrid(x, y)


def FindR(x,y):
    # r = np.sqrt(x**2 + (y/e)**2)
    r = np.sqrt((x*np.cos(theta) + y*np.sin(theta))**2 + ((x*np.sin(theta) - y*np.cos(theta))/e)**2)
    r[r <= 1] = 0
    return r

# distance at which stress = 1/1000 of the original value
zerodist = 2
g = -np.log(0.001)/zerodist
# g = 1

def StressDecay(q):
    sigma = 1250 * np.exp(-q*g)
    return sigma

R = FindR(X,Y)
stress = StressDecay(R-l1)

maskq = np.ma.masked_where(R == 0, stress)


levels = np.linspace(0, 21250, 25)

# plt.contour(X, Y, stress, levels)
plt.contourf(X,Y,maskq)
plt.colorbar()
plt.show()
