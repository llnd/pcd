"Constructs matrices from expansion coefficients."

import numpy as np

def bmn(m,n,dp,dq,v0p,v0q,Mu,Eta,nmax,mmax,zpq,ethp,ethq,ethpq):
    """
    Returns the coefficients for one pq pair, for bmnpq: bnq = sum of (Amp * bmnpqa) + (Bmp * bmnpqb).
    This is done for each m and n up to n-1 (because of indexing limits).


    Parameters
    -------
    nmax, mmax : integers
        Accuracy as set in the main function
    m,n : arrays
        Integer arrays up to nmax and mmax to loop/ broadcast over
    dp,dq : float
        The inter-foci distance of each inclusion under consideration
    v0p,v0q : float
        The v0 = exp(Z0) for each inclusion under consideration
    Mu, Eta : arrays
        The expansion coefficients for one pair: i.e. Eta[h] or Mu[h] (see
        function bmnpq) - just Eta/Mu for one pq, not the entire nested Eta/Mu array.
    zpq : array
        The distance between the centers of each pq pair (zcenterq - zcenterp)
    ethp : float
        exp(i * thetaP)
    ethq : float
        exp(i * thetaQ)
    ethpq : float
        exp(i * thetaPQ)


    Returns
    -------
    bmnpqa : array
        The multiplier for Amp for calculating the bnpq coefficient
    bmnpqb : array
        The multiplier for Bmp for calculating the bnpq coefficient
    smnpq : array
        The multiplier for Amp for calculating the anpq coefficient

    """
    smnpq = Eta[:,:-1] * ethpq**(-1)
    bmnpqb = Eta[:,:-1] * ethpq
    bmnpqa = []
    for i, m_value in enumerate(m):
        for w, n_value in enumerate(n[:-1]):        # only up to n-1 so that i can index n+1 later without it going weird
            # limiting range of k so that the index is within bounds
            kmax = int(0.5*(mmax-m_value))
            if 0.5*(nmax-n_value-1) < 0:        # slightly crude way for now of solving the problem where int(-.5=0)
                k = np.asarray([])
            else:
                k = np.arange(0, kmax+1, 1)

            bmnpqa_1 = Mu[i][w+1] * -0.5*dp * n_value/(n_value+1) * (v0p - 1/v0p)**2 * ethpq
            bmnpqa_2 = (m_value*(1 - v0q**(-2))*(ethpq**(-1)) - n_value*(1-v0p**(-2))*ethpq) * Eta[i][w]      # trying with negative n and not... not much diff * ethpq
            bmnpqa_3 = (zpq.conjugate()*ethq - zpq*(ethq**(-1))) * ethp * Mu[i][w]  # need to change the thetas
            if w == 0:
                bmnpqa_4a = Mu[i][w+1]
            else:
                bmnpqa_4a = Mu[i][w-1] + Mu[i][w+1]
            bmnpqa_4 = (bmnpqa_4a * 0.5*dp - Eta[i][w]) * (ethpq**(-1) - ethpq)
            # find the sum over k for each mn
            sum_k = []
            for u, k_value in enumerate(k):
                """k_terms: each k term that needs to be in the summation
                +n instead of -n because n<0
                Eta[m][n] is indexed as Eta[m-1][n-1] - eg Eta11 = Eta[0][0],
                hence must add a -1 on the index if indexing by value"""
                k_terms = Eta[2*k_value+m_value -1][w] * (2*k_value + m_value)
                sum_k.append(k_terms)
            sumk = np.sum(sum_k)                  # k sum term
            bmnpqa_5 = sumk*(v0q - 1/v0q)**2 * (ethpq**(-1))

            bmnpqa_terms = bmnpqa_1 + bmnpqa_2 + bmnpqa_3 + bmnpqa_4 + bmnpqa_5
            bmnpqa.append(bmnpqa_terms)

    bmnpqa = np.asarray(bmnpqa).reshape(len(m),len(n)-1)
    return bmnpqa, bmnpqb, smnpq

def bmnpq(m,n,nmax,mmax,pq,d,v0,Eta,Mu,zpq,eith,eithpq):
    """
    Loops to find bmn for each pq (see function bmn for more details on bmn).

    Parameters
    -------
    nmax, mmax : integers
        Accuracy as set in the main function
    m,n : arrays
        Integer arrays up to nmax and mmax to loop/ broadcast over
    pq : array
        Permuted pairs of inclusions
    d : array
        The inter-foci distance of the respective inclusions
    v0 : array
        The array of v0 = exp(Z0) for the respective inclusions
    Mu, Eta : arrays
        The expansion coefficients for all pairs (in the order of permutations in pq)
    zpq : array
        The distance between the centers of each pq pair (zcenterq - zcenterp)
    eith : array
        exp(i * theta) where theta has been converted to radians
    eithpq : array
        exp(i * thetapq) where thetapq is the difference in angle between each pair


    Returns
    -------
    bmnpqa : array
        The multiplier for Amp for calculating the bmnpq coefficient
    bmnpqb : array
        The multiplier for Bmp for calculating the bmnpq coefficient

    """
    bmnpqa, bmnpqb = [], []
    smnpq = []
    for h, pair in enumerate(pq):
        dp, dq = d[pair[0]], d[pair[1]]
        v0p, v0q = v0[pair[0]], v0[pair[1]]
        ethpq = eithpq[h]
        ethp, ethq = eith[pair[0]], eith[pair[1]]
        bmna, bmnb, smn = bmn(m,n,dp,dq,v0p,v0q,Mu[h],Eta[h],nmax,mmax,zpq[h],ethp,ethq,ethpq) # will have more thetas eventually!
        bmnpqa.append(bmna)
        bmnpqb.append(bmnb)
        smnpq.append(smn)
    bmnpqa = np.asarray(bmnpqa)
    bmnpqb = np.asarray(bmnpqb)
    smnpq = np.asarray(smnpq)
    return bmnpqa, bmnpqb, smnpq

def M12(dim, pq, n, v0, k0, k1, omega, Z0, Ntot):
    """
    Define matrix (M1) for coefficients An, Bn, Cn, Dn;
    and their conjugates (M2). M1 and M2 are for the LHS of the equations.

    Parameters
    -------
    dim : integer
        Size of the matrix (4 * Ntot * n), where n = nmax-1 because of indexing
    pq : array
        Permuted pairs of inclusions
    n : arrays
        Integer array up to nmax to loop/ broadcast over
    v0 : array
        The array of v0 = exp(Z0) for the respective inclusions
    k0 : float
        Kappa for the matrix
    k1 : array
        Kappa for each of the N inclusions
    omega : array
        Ratio of inclusion to matrix shear moduli G1/G0 for each inclusion
    Z0 : array
        Zeta0 for each inclsion, defined by geometry of the ellipse
    Ntot : integer
        Total number of inclusions in the system

    Returns
    -------
    M1 : array
        Matrix for ABCD for LHS of equations
    M2 : array
        Matrix for (ABCD.conjugate) for LHS of equations

    """
    M1 = np.zeros((dim,dim))
    M2 = np.zeros((dim,dim))
    for i, pair in enumerate(pq):
        q_i = pair[1]
        for w, n_value in enumerate(n[:-1]):
            v0q2 = v0[q_i]**(2*n_value)
            row = q_i*4+(4*Ntot)*w
            col = q_i*4+(4*Ntot)*w
            M1[row][col] = k0
            M1[row][col+2] = -k1[q_i]
            M1[row+1][col+2] = k1[q_i] * v0q2
            M1[row+2][col] = 1
            M1[row+2][col+2] = -omega[q_i]
            M1[row+3][col+2] = -omega[q_i] * v0q2

            M2[row,col+2] = -2 * n_value * v0q2 * np.sinh(2*Z0[q_i])
            M2[row+2,col+2] = 2 * n_value * omega[q_i] * v0q2 * np.sinh(2*Z0[q_i])
            M2[row,col+3] = v0q2
            M2[row+1,col+1] = 1
            M2[row+1,col+3] = -1
            M2[row+2,col+3] = -omega[q_i] * v0q2
            M2[row+3,col+3] = -omega[q_i]
            M2[row+3,col+1] = 1
    return M1, M2

def BCcoeffs(dim, pq, n, k0, v0, Ntot):
    """
    Coefficients for boundary conditions to multiply the expansion of ABCD on the RHS with;
    i.e. to multiply with M3/4 after adding the farfield boundary conditions to them.

    Parameters
    -------
    dim : integer
        Size of the matrix (4 * Ntot * n), where n = nmax-1 because of indexing
    pq : array
        Permuted pairs of inclusions
    n : arrays
        Integer array up to nmax to loop/ broadcast over
    k0 : float
        Kappa for the matrix
    v0 : array
        The array of v0 = exp(Z0) for the respective inclusions
    Ntot : integer
        Total number of inclusions in the system

    Returns
    -------
    coeffs : array
        Matrix of coefficients on the RHS of the equations

    """
    coeffs = np.zeros((dim,dim), dtype=complex)
    for i, pair in enumerate(pq):
        p_i = pair[0]
        q_i = pair[1]
        for w, n_value in enumerate(n[:-1]):
            v0q2 = v0[q_i]**(2*n_value)
            col = q_i*4+(4*Ntot)*w
            row = q_i*4+(4*Ntot)*w
            coeffs[row][col] = -k0
            coeffs[row][col+3] = v0q2
            coeffs[row+1][col+1] = -1
            coeffs[row+1][col+2] = k0 * v0q2
            coeffs[row+2][col] = -1
            coeffs[row+2][col+3] = -v0q2
            coeffs[row+3][col+1] = -1
            coeffs[row+3][col+2] = -v0q2
    return coeffs

def M34(dim, pq, n, v0, Ntot, smnpq, bmnpqa, bmnpqb):
    """
    Construct the overall matrices M3 and M4 for the boundary conditions due to other inclusions.
    Non-farfield BCs = coeffs . ( M3.(ABCD)n + M4.conj(ABCD)n )

    Note that for Amp/Anq etc, m and n are used as dummy integers. The extra m terms in the array are
    not used here (they were used for accuracy in calculating the ksum in bmnpqa).

    Parameters
    -------
    dim : integer
        Size of the matrix (4 * Ntot * n), where n = nmax-1 because of indexing
    pq : array
        Permuted pairs of inclusions
    n : arrays
        Integer array up to nmax to loop/ broadcast over
    v0 : array
        The array of v0 = exp(Z0) for the respective inclusions
    Ntot : integer
        Total number of inclusions in the system
    smnpq : array
        The multiplier for Amp for calculating the anpq coefficient
    bmnpqa : array
        The multiplier for Amp for calculating the bnpq coefficient
    bmnpqb : array
        The multiplier for Bmp for calculating the bnpq coefficient

    Returns
    -------
    M3 : array
        Matrix for ABCD for RHS of equations - boundary condition due to other inclusions
    M4 : array
        Matrix for (ABCD.conjugate) for RHS of equations  - boundary condition due to other inclusions

    """
    M3 = np.zeros((dim,dim), dtype=complex)
    M4 = np.zeros((dim,dim), dtype=complex)
    for i, pair in enumerate(pq):
        p_i = pair[0]
        q_i = pair[1]
        for v, m_value in enumerate(n[:-1]):
            for w, n_value in enumerate(n[:-1]):
                v0q2 = v0[q_i]**2
                row = q_i*4+(4*Ntot)*v
                col = p_i*4+(4*Ntot)*w
                M3[row][col] = M3[row+2][col] = smnpq[i][v][w]
                M4[row+1][col] = bmnpqa.conjugate()[i][v][w]
                M4[row+1][col+1] = M4[row+3][col+1] = bmnpqb.conjugate()[i][v][w]
                M4[row+3][col] = (bmnpqa[i][v][w] - m_value * (v0q2 - 1/v0q2) * smnpq[i][v][w]).conjugate()
    return M3, M4

def SolveCoeffs(rhs, M1, M2):
    """
    Solve for An, Bn, Cn, Dn.
    """
    # Find real part
    rhs_real = rhs.real
    lhs_real = M1 + M2
    real = np.linalg.solve(lhs_real, rhs_real)

    # Find imaginary part
    rhs_im = rhs.imag
    lhs_im = M1 - M2
    imag = np.linalg.solve(lhs_im, rhs_im)

    coeffs = 1j*imag; coeffs += real

    return coeffs
