#!/usr/bin/env python3

"Solving for the stress fields of two ellipses."

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
import matplotlib.cm as cm
from scipy.special import factorial
from itertools import permutations

def PhysConsts(N):
    """
    Defining physical constants:
    - matrix variables denoted (0), inclusion variables (1)
    - N: total number of inclusions in the matrix

    nu : poisson's ratio
    G : shear modulus

    For cobalt, nu = 0.31, G = 75 GPa;
    For diamond, nu = 0.07, G = 478 GPa;
    For a crack or hole, nu = G = 0.

    kappa = 3-4*nu for plane strain, and 3-4*nu/1+nu for plane stress.
    """

    # Matrix (Diamond)
    nu0 = 0.07
    G0 = 570
    k0 = 3-4*nu0

    # Inclusions (Cracks)
    nu1 = np.zeros(N)
    G1 = np.zeros(N)
    # k1 = np.zeros(N)
    k1 = 3-4*nu1

    return nu0, G0, k0, nu1, G1, k1

def FarFieldStress():
    """
    Defining 2D stress state from imposed far field load.
    [can extend with theta later]
    """
    S11 = 0
    S22 = 1
    S12 = 0

    return S11, S22, S12

def StartPicture(limit, zcenter, l1, l2, Ntot):
    """
    Visualise initial geometry/ setup of inclusions.
    """
    fig, ax = plt.subplots(figsize=(6,6))
    plt.ylim(-limit, limit)
    plt.xlim(-limit, limit)
    for i in range(Ntot):
        ell = Ellipse((zcenter[i],0),2*l1[i],2*l2[i], linewidth=2, fill=False)
        ax.add_patch(ell)
    plt.show()

def MakeXi(spacing, limit, d):
    x = np.linspace(-limit, limit,spacing)
    y = np.linspace(-limit,limit,spacing)
    X, Y = np.meshgrid(x, y)
    def z(x,y):
        return x + 1j * y
    z = z(X, Y)
    """
    The bit above can be reused for each inclusion (i think), but since xi requires
    d, each xi is different for each inclusion.
    I'm not sure how to do this except to use a for loop, but there's probably more
    efficient ways to do it.
    """
    xi = []
    for i in range(len(d)):
        xxi = np.arccosh(z/d[i])
        xi.append(xxi)
    xi = np.asarray(xi)
    return xi

def FindMnm(m, n, k, l, dpdq):
    """
    Finds Mnml for each m and n, summed for each (m,n) up to a given value of l.

    Returns an array:
    first row: m = 1, n = 1,2,3...
    second row: m = 2, n = 1,2,3...
    """
    Mnm = []
    for j in range(len(m)):
        Mnm_n = []
        for i in range(len(n)):
            mnm = dpdq**(2*k)/ (factorial(k) * factorial(l-k) * factorial(k+n[i]) * factorial(m[j]+l-k))
            Mnm_samem = np.sum(mnm)
            Mnm_n.append(Mnm_samem)
        Mnm.append(Mnm_n)
    return Mnm

def FindMnml(m,n,l,dpdq):
    """
    Finds Mnml for the entire range of m, n, l.

    Inputs:
    m : array of values of m to be evaluated
    n : array of values of n to be evaluated
    l : array of values of l to be evaluated
    dpdq : scalar, dp/dq

    Outputs:
    Mnml : nested array; Mnml[i] gives l = i; Mnml[i][j][k] has indices given by l = i, m = j, n = k

    """
    Mnml = []
    for i in range(len(l)):
        k = np.arange(0, l[i]+1, 1)
        Mnm = FindMnm(m,n,k,l[i],dpdq)
        Mnml.append(Mnm)
    return Mnml

def EtaFarPQ(m,n,l,Mnml,dp,dq,zpq):
    """
    Calculate the expansion coefficients for distant inclusions.

    Returns an array where the rows give the m index and the columns are the n index,
    so e.g. eta_pq_far[0] is m = 1.
    """
    eta_pq_far = []
    for i in range(len(m)):
        m_value = m[i]
        for w in range(len(n)):
            n_value = n[w]
            "to sum over l for each mn"
            nm = Mnml[:,i,w] # value of Mnml for a fixed mn, for varying l : length of nm is length of l)
            sum_mn = (dq**(2*l+m_value) * nm * factorial(n_value + m_value + 2*l -1)
                / (2*zpq)**(n_value + m_value + 2*l)) * (-1)**(m_value) * n_value * dp**(n_value)
            sum = np.sum(sum_mn)
            # print("m is ",m_value, "n is ",n_value)
            # print(sum)
            eta_pq_far.append(sum)
    eta_pq_far= np.asarray(eta_pq_far).reshape(len(m),len(n))
    return eta_pq_far

def MuFarPQ(m,n,l,Mnml,dp,dq,zpq):
    """
    Calculate the expansion coefficients for distant inclusions.

    Returns an array where the rows give the m index and the columns are the n index,
    so e.g. eta_pq_far[0] is m = 1.
    """
    mu_pq_far = []
    for i in range(len(m)):
        m_value = m[i]
        for w in range(len(n)):
            n_value = n[w]
            "to sum over l for each mn"
            nm = Mnml[:,i,w] # value of Mnml for a fixed mn, for varying l : length of nm is length of l)
            sum_mn = (dq**(2*l+m_value) * nm * factorial(n_value + m_value + 2*l)
                / (2*zpq)**(n_value + m_value + 2*l + 1)) * (-1)**(m_value) * (-2) * n_value * dp**(n_value)
            sum = np.sum(sum_mn)
            # print("m is ",m_value, "n is ",n_value)
            # print(sum)
            mu_pq_far.append(sum)
    mu_pq_far= np.asarray(mu_pq_far).reshape(len(m),len(n))
    return mu_pq_far


def EtaNearPQ(m,n,l,j,Mnml,dp,dq,dpq,vpq):
    """
    Calculate the expansion coefficients for near inclusions.

    Returns an array where the rows give the m index and the columns are the n index,
    so e.g. eta_pq_far[0] is m = 1.
    """
    eta_pq_near = []
    for i in range(len(m)):
        m_value = m[i]
        for w in range(len(n)):
            n_value = n[w]
            "to sum over l for each mn"
            # print("m is ",m_value, "n is ",n_value)
            sumj = []
            for v in range(len(j)):
                j_value = j[v]
                nm = Mnml[:j_value+1,i,w] # value of Mnml for a fixed mn, for varying l, up to l=j
                trunc_l = l[:j_value+1] # truncating l so that the sum is only evaluated up to l=j
                # print(factorial(m_value + n_value + trunc_l + j_value -1))
                sum_l = ((-1)**(j_value-trunc_l)
                        / factorial(j_value-trunc_l)
                        * (dq/dpq)**(2*trunc_l + m_value)
                        * nm
                        * factorial(m_value + n_value + trunc_l + j_value -1)
                        * vpq**(-(n_value + m_value + 2*j_value))
                        * n_value * (dp/dpq)**n_value
                        * (-1)**m_value)
                sum_j = np.sum(sum_l)       # this sums over l for each j
                # print(j_value, sum_l, sum_j)
                sumj.append(sum_j)
            sum = np.sum(sumj)         # this sums over j for each m/n
            # print(sum)
            eta_pq_near.append(sum)
    eta_pq_near= np.asarray(eta_pq_near).reshape(len(m),len(n))
    return eta_pq_near

def MuNearPQ(m,n,l,j,Mnml,dp,dq,dpq,vpq,vpqprime):
    """
    Calculate the expansion coefficients for near inclusions.

    Returns an array where the rows give the m index and the columns are the n index,
    so e.g. eta_pq_far[0] is m = 1.
    """
    mu_pq_near = []
    for i in range(len(m)):
        m_value = m[i]
        for w in range(len(n)):
            n_value = n[w]
            "to sum over l for each mn"
            # print("m is ",m_value, "n is ",n_value)
            sumj = []
            for v in range(len(j)):
                j_value = j[v]
                nm = Mnml[:j_value+1,i,w] # value of Mnml for a fixed mn, for varying l, up to l=j
                trunc_l = l[:j_value+1] # truncating l so that the sum is only evaluated up to l=j
                # print(factorial(m_value + n_value + trunc_l + j_value -1))
                sum_l = ((-1)**(j_value-trunc_l)
                        / factorial(j_value-trunc_l)
                        * (dq/dpq)**(2*trunc_l + m_value)
                        * vpqprime
                        * nm
                        * factorial(m_value + n_value + trunc_l + j_value -1)
                        * (n_value + m_value + 2*j_value) * vpq**(-(n_value + m_value + 2*j_value + 1))
                        * (-n_value) * (dp/dpq)**n_value
                        * (-1)**m_value)
                sum_j = np.sum(sum_l)       # this sums over l for each j
                # print(j_value, sum_l, sum_j)
                sumj.append(sum_j)
            sum = np.sum(sumj)         # this sums over j for each m/n
            # print(sum)
            mu_pq_near.append(sum)
    mu_pq_near= np.asarray(mu_pq_near).reshape(len(m),len(n))
    return mu_pq_near

def ExpansionCoeffs(m,n,l,j,zpq,dpq,dpdq,vpq,vpqprime,pq,Ntot,d):
    """
    Finds Eta and Mu for each pair, according to the order of permutations in pq.
    """
    Mnml_pq = []
    for pair in range(len(pq)):
        Mnml_pq.append(FindMnml(m,n,l,dpdq[pair]))
    Mnml_pq = np.asarray(Mnml_pq)
    # (have not yet written the integral forms/ separated by near/far conditions, bc i want to move on first)
    EpqFar, EpqNear, MuFar, MuNear = [], [], [], []
    for i, pair in enumerate(pq):
        dp, dq = d[pair[0]], d[pair[1]]
        EpqFar.append(EtaFarPQ(m,n,l,Mnml_pq[i],dp,dq,zpq[i]))
        EpqNear.append(EtaNearPQ(m,n,l,j,Mnml_pq[i],dp,dq,dpq[i],vpq[i]))
        MuFar.append(MuFarPQ(m,n,l,Mnml_pq[i],dp,dq,zpq[i]))
        MuNear.append(MuNearPQ(m,n,l,j,Mnml_pq[i],dp,dq,dpq[i],vpq[i],vpqprime[i]))
    EpqFar = np.asarray(EpqFar)
    EpqNear = np.asarray(EpqNear)
    MuFar = np.asarray(MuFar)
    MuNear = np.asarray(MuNear)
    return EpqFar, EpqNear, MuFar, MuNear

def bmn(m,n,dp,dq,v0p,v0q,Mu,Eta,nmax,mmax):
    """
    Returns the coefficients for bmnpq for one pq: bnq = sum of (Amp * bmnpqa) + (Bmp * bmnpqb).
    This is done for each m and n up to n-1 (because of indexing limits).

    Eta/Mu here is Eta[h]/Mu[h] (see function bmnpq), i.e. just the Eta for one pq pair.
    """
    bmnpqb = Eta[:,:-1]
    bmnpqa = []
    test = []
    for i, m_value in enumerate(m):
        for w, n_value in enumerate(n[:-1]):        # only up to n-1 so that i can index n+1 later without it going weird
            # limiting range of k so that the index is within bounds
            kmax = int(0.5*(mmax-m_value))
            if 0.5*(nmax-n_value-1) < 0:        # slightly crude way for now of solving the problem where int(-.5=0)
                k = np.asarray([])
            else:
                k = np.arange(0, kmax+1, 1)

            bmnpqa_1 = Mu[i][w+1] * -0.5*dp * n_value/(n_value+1) * (v0p - 1/v0p)**2
            bmnpqa_2 = (m_value*(1 - v0q**(-2)) - n_value*(1-v0p**(-2))) * Eta[i][w]      # trying with negative n and not... not much diff
            # bmnpqa_3 = (zpq[0]*np.exp(1j*theta)-zpq[0]*np.exp(-1j*theta))*np.exp(1j*theta)  # need to add the conjugate but havent yet cos it's real.. also need to change the thetas
            # bmnpqa_4 = Mu[i-1][w] + Mu[i+1][w]    # code this later but it's zero when theta is zero; need to edit m range also

            # find the sum over k for each mn
            sum_k = []
            for u, k_value in enumerate(k):
                """k_terms: each k term that needs to be in the summation
                +n instead of -n because n<0
                Eta[m][n] is indexed as Eta[m-1][n-1] - eg Eta11 = Eta[0][0], hence must add a -1 on the index if indexing by value"""
                k_terms = Eta[2*k_value+m_value -1][w] * (2*k_value + m_value)
                sum_k.append(k_terms)
            sumk = np.sum(sum_k)                  # k sum term
            bmnpqa_terms = bmnpqa_1 + bmnpqa_2 + sumk*(v0q - 1/v0q)**2
            bmnpqa.append(bmnpqa_terms)

    bmnpqa = np.asarray(bmnpqa).reshape(len(m),len(n)-1)
    return bmnpqa, bmnpqb

def bmnpq(m,n,nmax,mmax,pq,d,v0,Eta,Mu):
    """
    Finds bmn for each pq (see function bmn for more details on what bmn is).
    """
    bmnpqa, bmnpqb = [], []
    for h, pair in enumerate(pq):
        dp, dq = d[pair[0]], d[pair[1]]
        v0p, v0q = v0[pair[0]], v0[pair[1]]
        bmna, bmnb = bmn(m,n,dp,dq,v0p,v0q,Mu[h],Eta[h],nmax,mmax)
        bmnpqa.append(bmna)
        bmnpqb.append(bmnb)
    bmnpqa = np.asarray(bmnpqa)
    bmnpqb = np.asarray(bmnpqb)
    return bmnpqa, bmnpqb

def M12(dim, pq, n, v0, k0, k1, omega, Z0, Ntot):
    """
    Define matrix (M1) for coefficients An, Bn, Cn, Dn;
    and their conjugates (M1). M1 and M2 are for the LHS of the equations.
    """
    M1 = np.zeros((dim,dim))
    M2 = np.zeros((dim,dim))
    for i, pair in enumerate(pq):
        q_i = pair[1]
        for w, n_value in enumerate(n[:-1]):
            v0q2 = v0[q_i]**(2*n_value)
            row = q_i*4+(4*Ntot)*w
            col = q_i*4+(4*Ntot)*w
            M1[row][col] = k0
            M1[row][col+2] = -k1[q_i]
            M1[row+1][col+2] = k1[q_i] * v0q2
            M1[row+2][col] = 1
            M1[row+2][col+2] = -omega[q_i]
            M1[row+3][col+2] = -omega[q_i] * v0q2

            M2[row,col+2] = -2 * n_value * v0q2 * np.sinh(2*Z0[q_i])
            M2[row+2,col+2] = 2 * n_value * omega[q_i] * v0q2 * np.sinh(2*Z0[q_i])
            M2[row,col+3] = v0q2
            M2[row+1,col+1] = 1
            M2[row+1,col+3] = -1
            M2[row+2,col+3] = -omega[q_i] * v0q2
            M2[row+3,col+3] = -omega[q_i]
            M2[row+3,col+1] = 1
    return M1, M2

def BCcoeffs(dim, pq, n, k0, v0, Ntot):
    """
    Coefficients for non-farfield boundary conditions to multiply M3/4 with,
    after adding the farfield boundary conditions to them.
    """
    coeffs = np.zeros((dim,dim), dtype=complex)
    for i, pair in enumerate(pq):
        p_i = pair[0]
        q_i = pair[1]
        for w, n_value in enumerate(n[:-1]):
            v0q2 = v0[q_i]**(2*n_value)
            print(q_i, v0[q_i]**2)
            col = q_i*4+(4*Ntot)*w
            row = q_i*4+(4*Ntot)*w
            coeffs[row][col] = -k0
            coeffs[row][col+3] = v0q2
            coeffs[row+1][col+1] = -1
            coeffs[row+1][col+2] = k0 * v0q2
            coeffs[row+2][col] = -1
            coeffs[row+2][col+3] = -v0q2
            coeffs[row+3][col+1] = -1
            coeffs[row+3][col+2] = -v0q2
    return coeffs

def M34(dim, pq, n, v0, Ntot, Eta, bmnpqa, bmnpqb):
    """
    Construct the overall matrices M3 and M4 for the boundary conditions due to other inclusions.
    Non-farfield BCs = coeffs . ( M3.(ABCD)n + M4.conj(ABCD)n )

    Note that for Amp/Anq etc, m and n are used as dummy integers. The extra m terms in the array are
    not used here (they were used for accuracy in calculating the ksum in bmnpqa).
    """
    M3 = np.zeros((dim,dim), dtype=complex)
    M4 = np.zeros((dim,dim), dtype=complex)
    for i, pair in enumerate(pq):
        p_i = pair[0]
        q_i = pair[1]
        for v, m_value in enumerate(n[:-1]):
            for w, n_value in enumerate(n[:-1]):
                v0q2 = v0[q_i]**2           # NOT ^(2*n)
                # print(n_value, v0q2)
                row = q_i*4+(4*Ntot)*w
                col = p_i*4+(4*Ntot)*v
                M3[row][col] = M3[row+2][col] = Eta[i][v][w]
                M4[row+1][col] = bmnpqa.conjugate()[i][v][w]
                M4[row+1][col+1] = M4[row+3][col+1] = bmnpqb.conjugate()[i][v][w]
                M4[row+3][col] = bmnpqa.conjugate()[i][v][w] - n_value * (v0q2 - 1/v0q2) * Eta[i][v][w]
    return M3, M4

def SolveCoeffs(rhs, M1, M2):
    """
    Solve for An, Bn, Cn, Dn.
    """
    # Find real part
    rhs_real = rhs.real
    lhs_real = M1 + M2
    real = np.linalg.solve(lhs_real, rhs_real)

    # Find imaginary part
    rhs_im = rhs.imag
    lhs_im = M1 - M2
    imag = np.linalg.solve(lhs_im, rhs_im)

    coeffs = 1j*imag; coeffs += real

    return coeffs


def main():
    # Set up problem geometry
    N1 = 2                              # number of inclusions in the x direction
    N2 = 1                              # number of inclusions in the y direction
    Ntot = N1*N2                        # total number of inclusions
    zcenter = np.linspace(-2,2,Ntot)    # locations of inclusions
    S11, S22, S12 = FarFieldStress()    # far field stress
    l1 = np.full(Ntot, 1)               # major axis length
    # l2 = np.full(Ntot, 0.5)           # minor axis length
    l2 = np.asarray([0.4,0.2])
    e = l2/l1                           # aspect ratio
    d = np.sqrt(l1*l1 - l2*l2)          # inter-foci distance
    Z0 = 0.5 * np.log((1+e)/(1-e))
    v0 = np.exp(Z0)
    print('v0', v0)
    print('Z0', Z0)

    permute = np.arange(0,Ntot,1)           # starting from 0 so easier to index
    pq = list(permutations(permute,2))      # all pairs of pq
    print(pq)

    # testing = list(permutations([0,1,2,3],2))
    # print(testing)

    spacing = 3                       # fineness of mesh in calculating potentials
    limit = 4                           # boundaries of problem
    theta = 0
    Xi = MakeXi(spacing, limit, d)

    # StartPicture(limit, zcenter, l1, l2, Ntot)    # visualising geometry

    # Define physical constants
    nu0, G0, k0, nu1, G1, k1 = PhysConsts(Ntot)
    omega = G1/G0

    # Define accuracy...
    # ...for basis functions
    nmax = 3            # note that the actual accuracy is to nmax-1 because of indexing
    n = np.arange(1, nmax+1, 1)
    # ...for expansion coefficients
    m_accuracy = 10      # can use 10 later, but reducing now so code is easier
    mmax =  2 * m_accuracy + nmax
    m = np.arange(1, mmax+1, 1)
    lmax = 10
    l = np.arange(0, lmax+1, 1)
    jmax = 5
    j = np.arange(0, jmax+1, 1)

    """
    EXPANSION COEFFICIENTS
    """
    zpq, dpq, dpdq = [], [], []
    for pair in pq:
        p = pair[0]
        q = pair[1]
        zpq.append(zcenter[q] - zcenter[p])
        dpq.append(d[q]+d[p])
        dpdq.append(d[p]/d[q])
    zpq, dpq, dpdq = np.asarray(zpq), np.asarray(dpq), np.asarray(dpdq)
    zdpq = zpq/dpq
    vpq = zdpq + np.sqrt(zdpq**2 - 1)*np.sign(zdpq)         # to take the +/- square root as needed
    vpqprime = (1/dpq)*(1+ zdpq / (np.sqrt((zdpq)**2 - 1)*np.sign(zdpq) ) )       # i'm assuming the same thing will work for vpqprime but we'll see

    EpqFar, EpqNear, MuFar, MuNear = ExpansionCoeffs(m,n,l,j,zpq,dpq,dpdq,vpq,vpqprime,pq,Ntot,d)
    Eta = EpqFar
    Mu = MuFar

    """
    MATRIX
    """
    # Find coefficients for Amp, Bmp
    bmnpqa, bmnpqb = bmnpq(m,n,nmax,mmax,pq,d,v0,Eta,Mu)
    dim = 4 * Ntot * (nmax-1)           # dimension of matrix for BC due to other inclusions
    coeffs = BCcoeffs(dim, pq, n, k0, v0, Ntot)
    M1, M2 = M12(dim, pq, n, v0, k0, k1, omega, Z0, Ntot)
    M3, M4 = M34(dim, pq, n, v0, Ntot, Eta, bmnpqa, bmnpqb)
    M3_2 = np.dot(coeffs, M3)
    M4_2 = np.dot(coeffs, M4)
    M5 = M1 - M3_2
    M6 = M2 - M4_2

    # print(100*M3.round(decimals=5).real)
    #
    # print(100*(M3_2+M4_2).round(decimals=6).real)
    #
    # print('minus \n', 100*(M3_2-M4_2).round(decimals=6).real)


    print(coeffs.real.round(decimals=2))

    """
    FARFIELD
    all anq/bnq is zero except for n=1, so there are q (i.e. Ntot) non-zero terms.

    ab = anq, bnq.conjugate(), a-nq, b-nq.conjugate()
    """

    ab = np.zeros((dim,1),dtype=complex)
    a1q = (S11 + S22)/(8*G0) * d/2
    b1q_minus = a1q/(v0**2) + d/(8*G0) * (S22 - S11 + 2j*S12)
    b1q = b1q_minus + 2 * a1q * np.sinh(2*Z0)       # also *n but n=1
    for i, pair in enumerate(pq):
        q_i = pair[1]
        ab[q_i*4] = ab[q_i*4+2] = a1q[q_i]
        ab[q_i*4+1] = b1q.conjugate()[q_i]
        ab[q_i*4+3] = b1q_minus.conjugate()[q_i]

    bcbar = np.dot(coeffs,ab)

    """
    SOLVE
    """
    ABCD = SolveCoeffs(bcbar, M5, M6)
    # Rows: n
    # Columns: inclusions
    Amp = ABCD[::4].reshape(nmax-1, Ntot)
    Bmp = ABCD[1::4].reshape(nmax-1, Ntot)
    Cmp = ABCD[2::4].reshape(nmax-1, Ntot)
    Dmp = ABCD[3::4].reshape(nmax-1, Ntot)

    print(Amp)
    # print(Eta)

    anpq = []
    for i, pair in enumerate(pq):
        p_i = pair[0]
        # q_i = pair[1]
        print(pair)
        an = []
        for v, m_value in enumerate(n[:-1]):
            amn = []
            for w, n_value in enumerate(n[:-1]):
                # print(m_value, n_value)
                # print(Amp[v][p_i])      # Amp
                # print(Eta[i][v][w])    # Eta mn
                amn.append(Amp[w][p_i]*Eta[i][v][w])
            # print(amn)
            # print(np.sum(np.asarray(amn)))

    # print(Eta[0])
    # print(bmnpqa)

    """
    POTENTIALS
    """

    v = np.exp(Xi)
    n1 = n[:-1, np.newaxis]
    vn = v ** n1[:, np.newaxis, np.newaxis]
    v_n = v ** (-n1[:, np.newaxis, np.newaxis])
    Av_n = Amp[:,:, np.newaxis, np.newaxis] * v_n
    Bv_n = Bmp[:,:, np.newaxis, np.newaxis] * v_n
    vv0q = v/v0[:, np.newaxis, np.newaxis] + v0[:, np.newaxis, np.newaxis]/v
    sinhzx = np.sinh(Z0[:, np.newaxis, np.newaxis])/np.sinh(Xi)
    sv = sinhzx * vv0q
    nAv_n = n1[:, np.newaxis, np.newaxis] * Av_n

    PhiQs = np.sum(Av_n, axis=0)
    PsiQs = np.sum(Bv_n, axis=0) - np.sum(sv * nAv_n, axis=0)



    # nxi = Xi*n1[:, np.newaxis, np.newaxis]
    # Acoshnxi = Amp[:,:, np.newaxis, np.newaxis] * np.cosh(nxi)
    # Phi = 2 * np.sum(Acoshnxi, axis=0)
    # print(PhiQs)
    # print(PhiQs.shape)


















if __name__ == '__main__':
    main()
