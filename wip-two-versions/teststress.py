#!/usr/bin/env python3

"Solving for the stress fields of a single ellipse (crack and circle)."

import numpy as np
from autograd import grad
import numpy.ma as ma
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.patches import Ellipse
import matplotlib.cm as cm
from matplotlib.colors import LogNorm

def PhysConsts():
    """
    Defining physical constants:
    - matrix variables denoted (0), inclusion variables (1)

    nu : poisson's ratio
    G : shear modulus

    For cobalt, nu = 0.31, G = 75 GPa;
    For diamond, nu = 0.07, G = 478 GPa;
    For a crack or hole, nu = G = 0.

    kappa = 3-4*nu for plane strain, and 3-4*nu/1+nu for plane stress.
    """

    nu0 = 0.07
    nu1 = 0
    G0 = 570
    G1 = 0
    k0 = 3-4*nu0
    k1 = 3-4*nu1

    return nu0, nu1, G0, G1, k0, k1


def FarFieldStress():
    """
    Defining 2D stress state from imposed far field load.

    [can extend with theta later]
    """

    S11 = 0
    S22 = 1
