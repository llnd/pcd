#!/usr/bin/env python3

"Solving for the stress fields of two ellipses."

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
import matplotlib.cm as cm
from scipy.special import factorial
from itertools import permutations

def PhysConsts(N):
    """
    Defining physical constants:
    - matrix variables denoted (0), inclusion variables (1)
    - N: total number of inclusions in the matrix

    nu : poisson's ratio
    G : shear modulus

    For cobalt, nu = 0.31, G = 75 GPa;
    For diamond, nu = 0.07, G = 478 GPa;
    For a crack or hole, nu = G = 0.

    kappa = 3-4*nu for plane strain, and 3-4*nu/1+nu for plane stress.
    """

    # Matrix (Diamond)
    nu0 = 0.07
    G0 = 570
    k0 = 3-4*nu0

    # Inclusions (Cracks)
    nu1 = np.zeros(N)
    G1 = np.zeros(N)
    k1 = np.zeros(N)

    return nu0, G0, k0, nu1, G1, k1

def FarFieldStress():
    """
    Defining 2D stress state from imposed far field load.
    [can extend with theta later]
    """
    S11 = 0
    S22 = 1
    S12 = 0

    return S11, S22, S12

def StartPicture(limit, zcenter, l1, l2, Ntot):
    """
    Visualise initial geometry/ setup of inclusions.
    """
    fig, ax = plt.subplots(figsize=(6,6))
    plt.ylim(-limit, limit)
    plt.xlim(-limit, limit)
    for i in range(Ntot):
        ell = Ellipse((zcenter[i],0),2*l1[i],2*l2[i], linewidth=2, fill=False)
        ax.add_patch(ell)
    plt.show()

def MakeXi(spacing, limit, d):
    x = np.linspace(-limit, limit,spacing)
    y = np.linspace(-limit,limit,spacing)
    X, Y = np.meshgrid(x, y)
    def z(x,y):
        return x + 1j * y
    z = z(X, Y)
    """
    The bit above can be reused for each inclusion (i think), but since xi requires
    d, each xi is different for each inclusion.
    I'm not sure how to do this except to use a for loop, but there's probably more
    efficient ways to do it.
    """
    xi = []
    for i in range(len(d)):
        xxi = np.arccosh(z/d[i])
        xi.append(xxi)
    xi = np.asarray(xi)
    return xi

def FindMnm(m, n, k, l, dpdq):
    """
    Finds Mnml for each m and n, summed for each (m,n) up to a given value of l.

    Returns an array:
    first row: m = 1, n = 1,2,3...
    second row: m = 2, n = 1,2,3...
    """
    Mnm = []
    for j in range(len(m)):
        "This bit returns Mnm for each value of n. Index 0: n=1, index 1: n=2 etc."
        Mnm_n = []
        for i in range(len(n)):
            mnm = dpdq**(2*k)/ (factorial(k) * factorial(l-k) * factorial(k+n[i]) * factorial(m[j]+l-k))
            Mnm_samem = np.sum(mnm)
            Mnm_n.append(Mnm_samem)
        Mnm.append(Mnm_n)
    return Mnm

def FindMnml(m,n,l,dpdq):
    """
    Finds Mnml for the entire range of m, n, l.

    Inputs:
    m : array of values of m to be evaluated
    n : array of values of n to be evaluated
    l : array of values of l to be evaluated
    dpdq : scalar, dp/dq

    Outputs:
    Mnml : nested array; Mnml[i] gives l = i; Mnml[i][j][k] has indices given by l = i, m = j, n = k

    """
    Mnml = []
    for i in range(len(l)):
        k = np.arange(0, l[i]+1, 1)
        Mnm = FindMnm(m,n,k,l[i],dpdq)
        Mnml.append(Mnm)
    return Mnml

def EtaFarPQ_01(m,n,l,Mnml,d,zpq):
    """
    Calculate the expansion coefficients for distant inclusions.

    Returns an array where the rows give the m index and the columns are the n index,
    so e.g. eta_pq_far[0] is m = 1.
    """
    eta_pq_far = []
    for i in range(len(m)):
        m_value = m[i]
        for w in range(len(n)):
            n_value = n[w]
            "to sum over l for each mn"
            nm = Mnml[:,i,w] # value of Mnml for a fixed mn, for varying l : length of nm is length of l)
            sum_mn = (d[0]**(2*l+m_value) * nm * factorial(n_value + m_value + 2*l -1)
                / (2*zpq)**(n_value + m_value + 2*l)) * (-1)**(m_value) * n_value * d[1]**(n_value)
            sum = np.sum(sum_mn)
            # print("m is ",m_value, "n is ",n_value)
            # print(sum)
            eta_pq_far.append(sum)
    eta_pq_far= np.asarray(eta_pq_far).reshape(len(m),len(n))
    return eta_pq_far


def EtaFarPQ_10(m,n,l,Mnml,d,zpq):
    """
    Calculate the expansion coefficients for distant inclusions.

    Returns an array where the rows give the m index and the columns are the n index,
    so e.g. eta_pq_far[0] is m = 1.
    """
    eta_pq_far = []
    for i in range(len(m)):
        m_value = m[i]
        for w in range(len(n)):
            n_value = n[w]
            "to sum over l for each mn"
            nm = Mnml[:,i,w] # value of Mnml for a fixed mn, for varying l : length of nm is length of l)
            sum_mn = (d[1]**(2*l+m_value) * nm * factorial(n_value + m_value + 2*l -1)
                / (2*zpq)**(n_value + m_value + 2*l)) * (-1)**(m_value) * n_value * d[0]**(n_value)
            sum = np.sum(sum_mn)
            # print("m is ",m_value, "n is ",n_value)
            # print(sum)
            eta_pq_far.append(sum)
    eta_pq_far= np.asarray(eta_pq_far).reshape(len(m),len(n))
    return eta_pq_far

def EtaNearPQ(m,n,l,j,Mnml,d,dpq,vpq):
    """
    Calculate the expansion coefficients for near inclusions.

    Returns an array where the rows give the m index and the columns are the n index,
    so e.g. eta_pq_far[0] is m = 1.
    """
    eta_pq_near = []
    for i in range(len(m)):
        m_value = m[i]
        for w in range(len(n)):
            n_value = n[w]
            "to sum over l for each mn"
            # print("m is ",m_value, "n is ",n_value)
            sumj = []
            for v in range(len(j)):
                j_value = j[v]
                nm = Mnml[:j_value+1,i,w] # value of Mnml for a fixed mn, for varying l, up to l=j
                trunc_l = l[:j_value+1] # truncating l so that the sum is only evaluated up to l=j
                # print(factorial(m_value + n_value + trunc_l + j_value -1))
                sum_l = ((-1)**(j_value-trunc_l)
                        / factorial(j_value-trunc_l)
                        * (d[0]/dpq)**(2*trunc_l + m_value)
                        * nm
                        * factorial(m_value + n_value + trunc_l + j_value -1)
                        * vpq**(-(n_value + m_value + 2*j_value))
                        * n_value * (d[1]/dpq)**n_value
                        * (-1)**m_value)
                sum_j = np.sum(sum_l)       # this sums over l for each j
                # print(j_value, sum_l, sum_j)
                sumj.append(sum_j)
            sum = np.sum(sumj)         # this sums over j for each m/n
            # print(sum)
            eta_pq_near.append(sum)
    eta_pq_near= np.asarray(eta_pq_near).reshape(len(m),len(n))
    return eta_pq_near

def main():
    # Set up problem geometry
    N1 = 2                              # number of inclusions in the x direction
    N2 = 1                              # number of inclusions in the y direction
    Ntot = N1*N2                        # total number of inclusions
    zcenter = np.linspace(-2,2,Ntot)    # locations of inclusions
    S11, S22, S12 = FarFieldStress()    # far field stress
    l1 = np.full(Ntot, 1)               # major axis length
    # l2 = np.full(Ntot, 0.5)           # minor axis length
    l2 = np.asarray([0.4,0.2])
    e = l2/l1                           # aspect ratio
    d = np.sqrt(l1*l1 - l2*l2)          # inter-foci distance
    Z0 = 0.5 * np.log((1+e)/(1-e))
    v0 = np.exp(Z0)

    permute = np.arange(0,Ntot,1)           # starting from 0 so easier to index
    pq = list(permutations(permute,2))      # all pairs of pq
    # print(pq)

    # StartPicture(limit, zcenter, l1, l2, Ntot)    # visualising geometry

    spacing = 400                       # fineness of mesh in calculating potentials
    limit = 4                           # boundaries of problem
    # xi = MakeXi(spacing, limit, d)

    # Define physical constants
    nu0, G0, k0, nu1, G1, k1 = PhysConsts(Ntot)
    omega = G1/G0

    # Define accuracy (set m,n,l)
    # For basis functions
    nmax = 3
    n = np.arange(1, nmax+1, 1)
    # For expansion coefficients
    m_accuracy = 1     # use 10 later, but reducing now so code is easier
    mmax =  2 * m_accuracy + nmax
    m = np.arange(1, mmax+1, 1)
    lmax = 10
    l = np.arange(0, lmax+1, 1)
    jmax = 5
    j = np.arange(0, jmax+1, 1)
    #
    # zpq = []
    # dpq = []
    # vpq = []
    # for pair in pq:
    #     p = pair[0]
    #     q = pair[1]
    #     zpq.append(zcenter[q] - zcenter[p])
    #     dpq.append(d[p]+d[q])
    # zdpq = np.asarray(zpq)/np.asarray(dpq)
    # print(zdpq)
    # # vpq = zpq/dpq - np.sqrt((zpq/dpq)**2 - 1)   # minus because i want the negative square root
    # print(vpq)



    # expanding pth(1) inclusion into the qth(0) coordinate system
    "qth inclusion: index 0, pth inclusion: index 1"

    zpq = zcenter[0] - zcenter[1]
    dpq = d[0] + d[1]
    vpq = zpq/dpq - np.sqrt((zpq/dpq)**2 - 1)   # minus because i want the negative square root
    dpdq = d[1] / d[0]
    Mnml = np.asarray(FindMnml(m,n,l,dpdq))
    # Eta(mnpq)
    # have not yet written the integral form of Eta, bc i want to move on to the matrix and get that working first
    # The difference between EpqFar01 and EpqFar10 is just the d[0] or d[1], but I haven't rewritten it to be general for pq yet,
    # so I'm just leaving it as 01 and 10 first so it works for 2 inclusions. But then I'll have to modify
    # the matrix later too.
    EpqFar01 = EtaFarPQ_01(m,n,l,Mnml,d,zpq)
    # EpqNear = EtaNearPQ(m,n,l,j,Mnml,d,dpq,vpq)
    # Mu(mnpq)


    # print(EpqFar01)



    mu_pq_far = []
    for i in range(len(m)):
        m_value = m[i]
        for w in range(len(n)):
            n_value = n[w]
            "to sum over l for each mn"
            nm = Mnml[:,i,w] # value of Mnml for a fixed mn, for varying l : length of nm is length of l)
            sum_mn = (d[0]**(2*l+m_value) * nm * factorial(n_value + m_value + 2*l)
                / (2*zpq)**(n_value + m_value + 2*l + 1)) * (-1)**(m_value) * (-2) * n_value * d[1]**(n_value)
            sum = np.sum(sum_mn)
            # print("m is ",m_value, "n is ",n_value)
            # print(sum)
            mu_pq_far.append(sum)
    mu_pq_far= np.asarray(mu_pq_far).reshape(len(m),len(n))
    print(mu_pq_far)




    vpqprime = (1/dpq)*(1+ (zpq/dpq) / (-np.sqrt((zpq/dpq)**2 - 1)) )

    mu_pq_near = []
    for i in range(len(m)):
        m_value = m[i]
        for w in range(len(n)):
            n_value = n[w]
            "to sum over l for each mn"
            # print("m is ",m_value, "n is ",n_value)
            sumj = []
            for v in range(len(j)):
                j_value = j[v]
                nm = Mnml[:j_value+1,i,w] # value of Mnml for a fixed mn, for varying l, up to l=j
                trunc_l = l[:j_value+1] # truncating l so that the sum is only evaluated up to l=j
                # print(factorial(m_value + n_value + trunc_l + j_value -1))
                sum_l = ((-1)**(j_value-trunc_l)
                        / factorial(j_value-trunc_l)
                        * (d[0]/dpq)**(2*trunc_l + m_value)
                        * vpqprime
                        * nm
                        * factorial(m_value + n_value + trunc_l + j_value -1)
                        * (n_value + m_value + 2*j_value) * vpq**(-(n_value + m_value + 2*j_value + 1))
                        * (-n_value) * (d[1]/dpq)**n_value
                        * (-1)**m_value)
                sum_j = np.sum(sum_l)       # this sums over l for each j
                # print(j_value, sum_l, sum_j)
                sumj.append(sum_j)
            sum = np.sum(sumj)         # this sums over j for each m/n
            # print(sum)
            mu_pq_near.append(sum)
    mu_pq_near= np.asarray(mu_pq_near).reshape(len(m),len(n))





    # expanding (0) inclusion into the (1) coordinate system
    zpq = zcenter[1] - zcenter[0]
    dpq = d[1] + d[0]
    vpq = zpq/dpq - np.sqrt((zpq/dpq)**2 - 1)   # minus because i want the negative square root
    dpdq = d[0] / d[1]
    Mnml = np.asarray(FindMnml(m,n,l,dpdq))
    # Eta(mnpq)
    EpqFar10 = EtaFarPQ_10(m,n,l,Mnml,d,zpq)
    # print(EpqFar10)



















    """
    MATRIX
    """
    "without for now separating by near/far conditions, moving on to the matrix"

    Amp = np.ones([nmax,Ntot])
    Bmp = np.ones([nmax,Ntot])
    Cmp = np.ones([nmax,Ntot])
    Dmp = np.ones([nmax,Ntot])

    # for pq=01
    v0p = v0[1]
    v0q = v0[0]
    dq = d[0]
    smnpq = EpqFar01
    #
    # for i in range(len(m)-1):       # only up to m-1 so that i can index m+1 later without it going weird
    #     m_value = m[i]
    #     for w in range(len(n)):
    #         n_value = n[w]
    #         k1max = int(0.5*(nmax-n_value))
    #         k1 = np.arange(1, k1max+1, 1)
    #         k2max = int(0.5*(nmax-n_value-1))
    #         k2 = np.arange(0, k2max+1, 1)
    #         # print(k1)
    #         # print(k2)
    #
    #         bmnpqa = 0.5* m_value * (v0p - 1/v0p)**2 * smnpq[i+1][w]
    #         # print(smnpq[i+1])
    #         sum_k1 = []
    #         sum_k2 = []
    #         for u in range(len(k1)):
    #             k1_value = k1[u]
    #             # print(n_value, k1)
    #             # print(m_value, 2*k1_value, n_value, 2*k1_value - n_value)
    #             """k1_terms: each k1 term that needs to be in the summation
    #             +n instead of -n because n<0
    #             Eta[m][n] is indexed as Eta[m-1][n-1] - eg Eta11 = Eta[0][0], hence must add a -1 on the index if indexing by value"""
    #             k1_terms = smnpq [i][2*k1_value + n_value - 1] * (2*k1_value + n_value)
    #             sum_k1.append(k1_terms)
    #
    #         for v in range(len(k2)):
    #             k2_value = k2[v]
    #             # k2_terms = smnpq [i][2*k2_value + n_value]
    #             print(n_value, k2)
    #             """NEED TO FIX THIS because int(-0.5) = 0 when i want it to not even get to -0.5 - how did it even get negative??
    #             think about it again later!!!!"""
    #             # print(k2_terms)
    #             # print(n_value, k2_value, 2*k2_value + n_value +1)
    #
    #         sumk1 = np.sum(sum_k1)
    #         # print(m_value, n_value, sumk1)
    #
    #
    #             # print(k1, k1_value, n_value)
    #





if __name__ == '__main__':
    main()
