#!/usr/bin/env python3

"figuring out why np.exp(xi) doesnt give me the right answer"

import numpy as np


def main():

    d = np.array([0.91651514, 0.9797959])

    spacing = 3
    limit = 4
    x = np.linspace(-limit, limit,spacing)
    y = np.linspace(-limit,limit,spacing)
    X, Y = np.meshgrid(x, y)
    def z(x,y):
        return x + 1j * y
    z = z(X, Y)

    xi = []
    for i in range(len(d)):
        xxi = np.arccosh(z/d[i])
        xi.append(xxi)
    xi = np.asarray(xi)


    xi = (np.array([[
  [2.51325641-2.34963293j,  2.17949212-1.57079633j,  2.51325641-0.79195972j],
  [ 2.15322703+3.14159265j,  0.00000000+1.57079633j,  2.15322703+0.j        ],
  [ 2.51325641+2.34963293j,  2.17949212+1.57079633j , 2.51325641+0.79195972j]],

 [[ 2.44651048-2.3486959j ,  2.11452586-1.57079633j , 2.44651048-0.79289676j],
  [ 2.08450333+3.14159265j , 0.00000000+1.57079633j , 2.08450333+0.j        ],
  [ 2.44651048+2.3486959j ,  2.11452586+1.57079633j , 2.44651048+0.79289676j]]]))
    print(repr(xi))
    v = np.exp(xi)
    print(v)





if __name__ == '__main__':
    main()
