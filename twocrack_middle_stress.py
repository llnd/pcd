#!/usr/bin/env python3

"Solving for the stress fields of two ellipses."


from itertools import permutations
from mpl_toolkits.mplot3d import Axes3D
from scipy.special import factorial
from matplotlib.patches import Ellipse
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import time
# other .py files:
from setup import PhysConsts, FarFieldStress, StartPicture, MakeXi2
from EtaMu import ExpansionCoeffs
from matrix import bmnpq, BCcoeffs, M12, M34
from potentials import abnpq, b_npq, Potentials, Pot0

def SolveCoeffs(rhs, M1, M2):
    """
    Solve for An, Bn, Cn, Dn.
    """
    # Find real part
    rhs_real = rhs.real
    lhs_real = M1 + M2
    real = np.linalg.solve(lhs_real, rhs_real)

    # Find imaginary part
    rhs_im = rhs.imag
    lhs_im = M1 - M2
    imag = np.linalg.solve(lhs_im, rhs_im)

    coeffs = 1j*imag; coeffs += real

    return coeffs

def dz(potential, stepsize):
    """
    Calculate the derivative wrt z where z = x+iy.
    """
    dy, dx = np.gradient(potential,stepsize)
    dz = dx - 1j*dy
    return dz

def dz2(potential, hx, hy):
    """
    Calculate the derivative wrt z where z = x+iy, with different step sizes.
    """
    dy, dx = np.gradient(potential,hy,hx)
    dz = dx - 1j*dy
    return dz

def Rotate(s11, s22, s12, thetar):
    """
    Rotates the stress fields from the local yq to zq coordinate systems for each inclusion.
    """
    # thetar = -thetar
    cos = np.cos(thetar)
    cos2 = np.cos(thetar*2)
    cos = cos[:, np.newaxis, np.newaxis]
    cos2 = cos2[:, np.newaxis, np.newaxis]
    sin = np.sin(thetar)
    sin2 = np.sin(thetar*2)
    sin = sin[:, np.newaxis, np.newaxis]
    sin2 = sin2[:, np.newaxis, np.newaxis]

    sigma11 = s11 * cos**2 - 2*s12*cos*sin + s22 * sin**2
    sigma12 = s12 * cos2 + (s11 - s22)*cos*sin
    sigma22 = s22 * cos**2 + s11*sin**2 + s12 * sin2

    return sigma11, sigma12, sigma22


def main():
 #  ___ _      _ _           _
 # | __(_)_ _ (_) |_ ___    /_\  _ _ _ _ __ _ _  _
 # | _|| | ' \| |  _/ -_)  / _ \| '_| '_/ _` | || |
 # |_| |_|_||_|_|\__\___| /_/ \_\_| |_| \__,_|\_, |
 #                                            |__/
 #        __   ___         _         _
 #  ___ / _| |_ _|_ _  __| |_  _ __(_)___ _ _  ___
 # / _ \  _|  | || ' \/ _| | || (_-< / _ \ ' \(_-<
 # \___/_|   |___|_||_\__|_|\_,_/__/_\___/_||_/__/

    """
    Initialise problem geometry
    """
    # Set up problem geometry
    N1 = 2                              # number of inclusions in the x direction
    N2 = 1                              # number of inclusions in the y direction
    Ntot = N1*N2                        # total number of inclusions
    ang = 0                             # angle of applied stress (degrees)
    S11, S22, S12 = FarFieldStress(ang) # far field stress
    length1 = 1
    l1 = np.full(Ntot, length1)         # major axis length
    l2 = np.asarray([0.0001,0.0001])        # minor axis length
    # l0 = 2                            # distance apart the edges of the ellipses are
    # dl_ratio = 0.1                        # distance between cracks / crack length

    start = time.time()

    rat = []
    str = []
    for r in range(1,26,1):
        dl_ratio = r/100
        rat.append(dl_ratio)

        l0 = dl_ratio * 2 * length1
        zcenter = np.array([-(0.5*l0+length1)+0j, (0.5*l0+length1)+0j]) # valid for 2 cracks
        e = l2/l1                           # aspect ratio
        theta = np.asarray([0,0])           # tilt angle in degrees
        thetar = theta/180 * np.pi          # tilt angle in radians
        eith = np.exp(1j*thetar)
        d = np.sqrt(l1*l1 - l2*l2)*eith     # inter-foci distance
        Z0 = 0.5 * np.log((1+e)/(1-e))
        v0 = np.exp(Z0)
        # print(Z0)

        # Create list of permuted pairs of inclusions (subsequent pq interaction terms use this order)
        permute = np.arange(0,Ntot,1)       # starting from 0 so easier to index
        pq = list(permutations(permute,2))  # all pairs of pq

        """
        For plotting the combined stress field, it is convenient for this setup to
        define the arrays (on which the stresses are calculated) individually, so that
        to superimpose the stresses, the arrays can just be added without indexing the
        relative displacements.
        """

        spacing = 401                       # fineness of mesh in calculating potentials
        limit = 0.2                          # boundaries of each local z coordinate system

        # limits for zeta and eta in elliptical coordinates
        limx = 5        # in multiples of Z0; spacing-1 must be divisible by limx to index Z0
        limy = np.pi

        # Visualising starting geometry
        # StartPicture(4, zcenter, l1, l2, theta, Ntot)

        EllCoords = False        # if True, Combine must = False (at least for now)
        Combine = True

        # Creating Xi for each inclusion (xi = zeta + i*eta)

        X, Y, X0, Y0, X1, Y1, Xi, zq, size = MakeXi2(limit, spacing, l0, length1, d)
        h = 2*2*size/(spacing)

        """
        Initialise physical constants and specify accuracy
        """

        # Define physical constants
        nu0, G0, k0, nu1, G1, k1 = PhysConsts(Ntot)
        omega = G1/G0                       # defined for eventual convenience in calculations

        # Set accuracy
        # -for basis functions
        nmax = 28                            # (IMPT!! the actual n accuracy is to nmax-1 because of indexing requirements later)
        # -for expansion coefficients
        m_accuracy = 10
        mmax =  2 * m_accuracy + nmax
        lmax = 50
        jmax = 10
        # Create arrays that can be looped over/ broadcasted
        n = np.arange(1, nmax+1, 1)
        m = np.arange(1, mmax+1, 1)
        l = np.arange(0, lmax+1, 1)
        j = np.arange(0, jmax+1, 1)

        # Find geometry specific terms
        zpq, dpq, dpdq, thetapq = [], [], [], []
        for pair in pq:
            p = pair[0]
            q = pair[1]
            zpq.append(zcenter[q] - zcenter[p])
            dpq.append(d[q]+d[p])
            dpdq.append(d[p]/d[q])
            thetapq.append(theta[q] - theta[p])
        zpq, dpq, dpdq, thetapq = np.asarray(zpq), np.asarray(dpq), np.asarray(dpdq), np.asarray(thetapq)
        zdpq = zpq/dpq
        vpq = zdpq + np.sqrt(zdpq**2 - 1)*np.sign(zdpq)         # to take the +/- square root as needed
        vpqprime = (1/dpq)*(1+ zdpq / (np.sqrt((zdpq)**2 - 1)*np.sign(zdpq)))
        eithpq = np.exp(1j*thetapq/180*np.pi)

        """
        Expansion Coefficients
        """

        Near = True             # calculates Eta/Mu_Near if true and Eta/Mu_Far if False
        Eta, Mu = ExpansionCoeffs(Near,m,n,l,j,zpq,dpq,dpdq,vpq,vpqprime,pq,Ntot,d)

        """
        Matrix
        """

        # Find coefficients for Amp, Bmp
        bmnpqa, bmnpqb, smnpq = bmnpq(m,n,nmax,mmax,pq,d,v0,Eta,Mu,zpq,eith,eithpq)
        # Dimension of the matrix
        dim = 4 * Ntot * (nmax-1)
        # Defining matrix for the (non-Amp) coefficients of the boundary conditions (RHS of the equations)
        coeffs = BCcoeffs(dim, pq, n, k0, v0, Ntot)
        # Matrix for the LHS of the equations
        M1, M2 = M12(dim, pq, n, v0, k0, k1, omega, Z0, Ntot)
        # Matrix for assembling the A/Bmp expansion of abmnpq coefficients (RHS of the equations)
        M3, M4 = M34(dim, pq, n, v0, Ntot, smnpq, bmnpqa, bmnpqb)
        # Assembled RHS of the equations
        M3_2 = np.dot(coeffs, M3)
        M4_2 = np.dot(coeffs, M4)
        # Creating final matrices that can be solved for x in the form of Mx = b.
        M5 = M1 - M3_2
        M6 = M2 - M4_2

        """
        Farfield Boundary Conditions

        All anq/bnq is zero except for n=1, so there are q (i.e. Ntot) non-zero terms.
        ab = [anq, bnq.conjugate(), a-nq, b-nq.conjugate()]
        """

        ab = np.zeros((dim,1),dtype=complex)
        a1q = (S11 + S22)/(8*G0) * d/2 * eith**(-1)
        b1q_minus = a1q/(v0**2) + d/(8*G0) * (S22 - S11 + 2j*S12) * eith
        b1q = b1q_minus + 2 * a1q * np.sinh(2*Z0)
        for i, pair in enumerate(pq):
            q_i = pair[1]
            ab[q_i*4] = ab[q_i*4+2] = a1q[q_i]
            ab[q_i*4+1] = b1q.conjugate()[q_i]
            ab[q_i*4+3] = b1q_minus.conjugate()[q_i]
        bcbar = np.dot(coeffs,ab)

        """
        Solve for [ABCD]mp
        """
        ABCD = SolveCoeffs(bcbar, M5, M6)

        # Rows correspond to n values, and columns correspond to inclusions by index.
        Amp = ABCD[::4].reshape(nmax-1, Ntot)
        Bmp = ABCD[1::4].reshape(nmax-1, Ntot)
        Cmp = ABCD[2::4].reshape(nmax-1, Ntot)
        Dmp = ABCD[3::4].reshape(nmax-1, Ntot)

        """
        Potentials
        """
        v = np.exp(Xi)
        n1 = n[:-1, np.newaxis]

        anpq, bnpq = abnpq(pq, n, Amp, Bmp, smnpq, bmnpqa, bmnpqb)
        bnpqminus = b_npq(pq, Z0, n1, anpq, bnpq)

        PhiQs, PsiQs, PhiPQr, PsiPQr, PhiFarQ, PsiFarQ = Potentials(v, n1, Amp, Bmp, v0, Z0, Xi, pq, anpq, bnpq, bnpqminus, a1q, b1q, b1q_minus)
        Phi0, Psi0 = Pot0(Ntot, pq, PhiPQr, PsiPQr, PhiQs, PsiQs)
        Phi = Phi0 + PhiFarQ
        Psi = Psi0 + PsiFarQ

        # Calculate derivatives of potentials
        wprime = d[:, np.newaxis, np.newaxis] * np.sinh(Xi)

        dPsi, dPhi, d2Phi = [], [], []

        if EllCoords == False:
            for i in range(Ntot):
                dpsi = dz(Psi[i], h)
                dphi = dz(Phi[i], h)
                ddphi = dz(dphi, h)
                dPsi.append(dpsi)
                dPhi.append(dphi)
                d2Phi.append(ddphi)
            dPsi = np.asarray(dPsi)
            dPhi = np.asarray(dPhi)
            d2Phi = np.asarray(d2Phi)

        """
        Stresses
        """

        sum = 4 * G0 * (dPhi + dPhi.conjugate()).real
        rhs = 4 * G0 * ((zq.conjugate()-zq) * d2Phi - dPhi + dPsi)
        diff = rhs.real
        s12 = 0.5 * rhs.imag
        s22 = (0.5 * (sum+diff))
        s11 = (0.5 * (sum-diff))

        # print(s22[0])
        # print(s11[0])
        # sigma11, sigma12, sigma22 = Rotate(s11,s22,s12,thetar)

        sigma11 = s11
        sigma12 = s12
        sigma22 = s22

        lim = 1.5
        levels = np.linspace(-lim, lim, 27)

        if Combine == True:
            # Superimposing the stresses (only if original arrays were defined with the right relative displacement!)
            s12tot = (sigma12[0]+sigma12[1])*0.5
            s11tot = (sigma11[0]+sigma11[1])*0.5
            s22tot = (sigma22[0]+sigma22[1])*0.5
            "line plots"

            middle = int(spacing*0.5)
            print(s22tot[middle,middle])
            # x = np.linspace(-size, size, spacing)
            # print(np.shape(x))
            # print(np.shape(s22tot))
            # # plt.xlim([0.5*size-0.5*l0,0.5*size+0.5*l0])
            # plt.xlim([-0.5*l0,0.5*l0])
            # # plt.scatter(x,s12tot[middle:,middle],marker='.', label='s12')
            # # plt.scatter(x,s12tot[:,middle],marker='.', label='s12')
            # # plt.scatter(x,s11tot[:,middle],marker='.', label='s11')
            # plt.scatter(x,s22tot[:,middle],marker='.', label='s22')
            # # plt.title("Stresses along the center line")
            # plt.legend()
            # plt.show()
            str.append(s22tot[middle,middle])

    end = time.time()

    print(rat)
    print(str)
    plt.plot(rat,str)
    plt.title("S22")
    plt.show()

    print("spacing:", spacing)
    print("limit:", limit)
    print("l1:", length1)
    print("n:", nmax-1)
    print("total time (s):", end-start)


if __name__ == '__main__':
    main()
