"Calculates potentials and the necessary coefficients for them."

import numpy as np

def abnpq(pq, n, Amp, Bmp, smnpq, bmnpqa, bmnpqb):
    """
    Returns anpq and bnpq, with rows giving different values of n,
    and columns corresponding to the pairs of pq.
    """
    anpq = []
    bnpq = []
    for i, pair in enumerate(pq):
        p_i = pair[0]
        an = []
        bn = []
        for v, m_value in enumerate(n[:-1]):
            amn = []
            bmn = []
            for w, n_value in enumerate(n[:-1]):
                # print(m_value, n_value)
                amn.append(Amp[w][p_i]*smnpq[i][v][w])
                bmn.append(Bmp[w][p_i]*bmnpqb[i][v][w] + Amp[w][p_i]*bmnpqa[i][v][w])
            an.append(np.sum(np.asarray(amn)))
            bn.append(np.sum(np.asarray(bmn)))
        anpq.append(an)
        bnpq.append(bn)
    anpq = np.transpose(np.asarray(anpq))
    bnpq = np.transpose(np.asarray(bnpq))
    return anpq, bnpq

def b_npq(pq, Z0, n1, anpq, bnpq):
    """
    Defines bnpqminus; n1 is an array of nmax-1 values for array broadcasting purposes.
    """
    sinhZ0 = []
    for i, pair in enumerate(pq):
        q_i = pair[1]
        sinhZ0.append(np.sinh(2*Z0[q_i]))
    sinhZ0 = np.asarray(sinhZ0)
    bnpqminus = bnpq - sinhZ0 * 2 * n1 * anpq
    return bnpqminus

def Potentials(Exp, v, n1, Amp, Bmp, v0, Z0, Xi, pq, anpq, bnpq, bnpqminus, a1q, b1q, b1q_minus):
    """
    Finds complex potentials for each pair pq/ each q inclusion.

    Exp :   0 if computing far stresses, 1 if computing near stresses
    v   :   np.exp(Xi)
    n1  :   list of n values used in calculating anpq and bnpq (ultimately runs up to nmax-2 bc of indexing)
    Amp :   expansion coefficients for each mp; rows correspond to n/m, columns correspond to inclusions
    v0  :   nested array, where v0[q] gives v0 for each qth inclusion
    Z0  :   nested array, where Z0[q] gives Z0 for each qth inclusion
    Xi  :   nested array containing xi[q] for each inclusion (Xi[0] is for q=0-th inclusion)
    pq  :   permutation pairs of all inclusions
    anpq:   expansion coefficients for expanding pth potentials into the qth coordinate basis; anpq[0] is for the pq[0] pair
    a1q :   farfield anq, but only n= +-1 is non-zero

    PhiQs:
        the singular part of the potential phi in the matrix due to inclusion Q
    PsiQs:
        the singular part of the potential psi in the matrix due to inclusion Q
    PhiPQr:
        expanding phi for the pth inclusion into the qth coordinate basis for each pair pq
    PsiPQr:
        expanding psi for the pth inclusion into the qth coordinate basis for each pair pq
    PhiFarQ:
        phi for the qth inclusion due to the far field
    PsiFarQ:
        psi for the qth inclusion due to the far field

    """

    vn = v ** n1[:, np.newaxis, np.newaxis]
    v_n = v ** (-n1[:, np.newaxis, np.newaxis])
    Av_n = Amp[:,:, np.newaxis, np.newaxis] * v_n
    Bv_n = Bmp[:,:, np.newaxis, np.newaxis] * v_n
    if Exp == False:
        vn1 = np.zeros_like(vn)
        vn1[0,:] = vn[0,:]
        vn = vn1

    vv0q = v/v0[:, np.newaxis, np.newaxis] - v0[:, np.newaxis, np.newaxis]/v
    sinhzx = np.sinh(Z0[:, np.newaxis, np.newaxis])/np.sinh(Xi)
    sinhzx2 = np.sinh(Xi - Z0[:, np.newaxis, np.newaxis]) * sinhzx
    sv = sinhzx * vv0q
    nAv_n = n1[:, np.newaxis, np.newaxis] * Av_n

    PhiQs = np.sum(Av_n, axis=0)
    PsiQs = np.sum(Bv_n, axis=0) - np.sum(sv * nAv_n, axis=0)

    # nxi = Xi*n1[:, np.newaxis, np.newaxis]        # vn + v_n = 2cosh(nxi)
    PhiPQr = []
    PsiPQr = []
    for i, pair in enumerate(pq):
        q_i = pair[1]       # need to select the right vq for whichever is the qth inclusion in each pair
        anpqvq = anpq[:,i, np.newaxis, np.newaxis] * (Exp*vn[:,q_i] + v_n[:,q_i])
        bnpqvq = bnpq[:,i, np.newaxis, np.newaxis] * v_n[:,q_i] + bnpqminus[:,i, np.newaxis, np.newaxis] * Exp*vn[:,q_i]
        psi0 = np.sum(bnpqvq, axis=0)
        anpq_vq = anpq[:,i, np.newaxis, np.newaxis] * (v_n[:,q_i] - Exp*vn[:,q_i])
        psi1 = np.sum(2 * n1[:, np.newaxis] * anpq_vq * sinhzx2[q_i], axis=0)
        PhiPQr.append(np.sum(anpqvq, axis=0))
        PsiPQr.append(psi0-psi1)
    PhiPQr = np.asarray(PhiPQr)
    PsiPQr = np.asarray(PsiPQr)

    # since all but n=1 are zero,
    PhiFarQ = a1q[:, np.newaxis, np.newaxis] * (v+1/v)
    PsiFarQ = ((b1q[:, np.newaxis, np.newaxis] - 2 * sinhzx2 * a1q[:, np.newaxis, np.newaxis]) * 1/v
                + (b1q_minus[:, np.newaxis, np.newaxis] + 2 * sinhzx2 * a1q[:, np.newaxis, np.newaxis]) * v)

    return PhiQs, PsiQs, PhiPQr, PsiPQr, PhiFarQ, PsiFarQ

def Pot0(Ntot, pq, PhiPQr, PsiPQr, PhiQs, PsiQs):
    """
    Generate Phi0 and Psi0. Pot0 = PotQs + Sum(PotPQr[p,q], for p up to Ntot)
    """
    SumPhiQr = []
    SumPsiQr = []
    for j in range(Ntot):
        # creating masked arrays, for each inclusion (number j)
        MaskQPhiPQr = np.ma.array(PhiPQr, mask=True)
        MaskQPsiPQr = np.ma.array(PsiPQr, mask=True)
        for i, pair in enumerate(pq):
            q_i = pair[1]       # the inclusion being looked at in this pair
            if q_i == j:
                # unmasking the parts of the array relevant for summation,
                # i.e. when q_i coincides with the inclusion currently being looked at
                MaskQPhiPQr.mask[i] = False
                MaskQPsiPQr.mask[i] = False
        SumPhiQr.append(np.sum(MaskQPhiPQr, axis=0))
        SumPsiQr.append(np.sum(MaskQPsiPQr, axis=0))
    SumPhiQr = np.asarray(SumPhiQr)
    SumPsiQr = np.asarray(SumPsiQr)

    Phi0 = PhiQs + SumPhiQr
    Psi0 = PsiQs + SumPsiQr

    return Phi0, Psi0
