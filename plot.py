"Plotting functions for stresses"

from matplotlib.patches import Ellipse
import matplotlib.pyplot as plt
import matplotlib.cm as cm

def plotc(X, Y, fn, name, levels, l1, l2, theta):
    """
    Make a contour plot of stresses for a single inclusion, at specified contours, without edges.

    (Edges are excluded because the numerical differentiation isn't accurate at the edges.)
    """
    # fig, ax = plt.subplots(figsize=(6,6))
    fig, ax = plt.subplots()
    # Contour fill plot
    contour = plt.contourf(X[1:-1,1:-1],Y[1:-1,1:-1],fn[1:-1,1:-1], levels, cmap=cm.coolwarm,zorder=1)
    # Contour line plot
    plt.contour(X[1:-1,1:-1],Y[1:-1,1:-1],fn[1:-1,1:-1], levels, cmap=cm.bwr,zorder=2,alpha=0.6)
    plt.axis('equal')
    plt.colorbar(contour,fraction=0.046, pad=0.04)
    plt.title("{}".format(name))
    # Add ellipse
    ell = Ellipse((0,0),2*l1,2*l2,theta,linewidth=1, fill=True, facecolor='w', edgecolor='k',zorder=10)
    ax.add_patch(ell)
    plt.show()

def plot2(X, Y, fn, name, levels, zcenter, l1, l2, theta):
    """
    Make a contour plot of stresses for more than one inclusion, at specified contours, without edges.

    (Edges are excluded because the numerical differentiation isn't accurate at the edges.)
    """
    # fig, ax = plt.subplots(figsize=(6,6))
    fig, ax = plt.subplots()
    # Contour fill plot
    contour = plt.contourf(X[1:-1,1:-1],Y[1:-1,1:-1],fn[1:-1,1:-1], levels, cmap=cm.coolwarm,zorder=1)
    # Contour line plot
    plt.contour(X[1:-1,1:-1],Y[1:-1,1:-1],fn[1:-1,1:-1], levels, cmap=cm.bwr,zorder=2,alpha=0.6)
    plt.axis('equal')
    plt.colorbar(contour,fraction=0.046, pad=0.04)
    plt.title("{}".format(name))
    # Add ellipse
    for i in range(len(l1)):
        ell = Ellipse((zcenter[i].real,zcenter[i].imag),2*l1[i],2*l2[i],theta[i], linewidth=1, fill=True, facecolor='w', edgecolor='k',zorder=10)
        ax.add_patch(ell)
    plt.show()

def PerimeterStress(sigma11, sigma12, sigma22, Z0_index, xi_eta, q):
    """
    Plots stresses around the perimeter of an inclusion q
    """
    s11_Z0 = sigma11[:,:,Z0_index]
    s12_Z0 = sigma12[:,:,Z0_index]
    s22_Z0 = sigma22[:,:,Z0_index]
    fig, axarr = plt.subplots(3, sharex=True, figsize=((6,8)))
    axarr[0].plot(xi_eta,s11_Z0[q],label='S11',color='r')
    axarr[0].set_title('S11')
    axarr[0].axhline(y=0, linestyle='--', linewidth=0.5, color='k')
    axarr[1].plot(xi_eta,s12_Z0[q],label='S12',color='g')
    axarr[1].set_title('S12')
    axarr[1].axhline(y=0, linestyle='--', linewidth=0.5, color='k')
    axarr[2].plot(xi_eta,s22_Z0[q],label='S22')
    axarr[2].set_title('S22')
    axarr[2].axhline(y=0, linestyle='--', linewidth=0.5, color='k')
    plt.show()
