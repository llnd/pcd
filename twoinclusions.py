#!/usr/bin/env python3

"Solving for the stress fields of two ellipses."

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
import matplotlib.cm as cm
from scipy.special import factorial
from itertools import permutations
import time
from EtaMu import ExpansionCoeffs

def PhysConsts(Ntot):
    """
    Defines physical constants - poisson's ratio (nu), shear modulus (G),
    and kappa (k).

    Kappa is a parameter used in complex variable elasticity, = 3-4*nu for
    plane strain, and 3-4*nu/1+nu for plane stress. Matrix variables are denoted
    with (0), inclusion variables with (1).

    For cobalt, nu = 0.31, G = 75 GPa;
    For diamond, nu = 0.07, G = 478 GPa;
    For a crack or hole, nu = G = 0.

    Parameters
    ----------
    Ntot : int
        The total number of inclusions in the matrix.

    Returns
    -------
    nu0 : float
        The poisson's ratio of the matrix
    G0 : float
        The shear modulus of the matrix (in GPa)
    k0 : float
        Kappa for the matrix in plane strain
    nu1 : array
        The poisson's ratio for each of the N inclusions
    G1 : array
        The shear modulus for each of the N inclusions (in GPa)
    k1 : array
        Kappa for each of the N inclusions in plane strain

    """

    # Matrix (Diamond)
    nu0 = 0.07
    G0 = 570
    k0 = 3-4*nu0

    # Inclusions (Cracks)
    nu1 = np.zeros(Ntot)
    G1 = np.zeros(Ntot)
    k1 = 3-4*nu1

    return nu0, G0, k0, nu1, G1, k1

def FarFieldStress():
    """
    Defines the 2D stress state from an imposed far field load.
    [can be extended with theta later]

    Returns
    -------
    S11 : float
        The stress in the x-direction
    S22 : float
        The stress in the y-direction
    S12 : float
        The stress in the xy-direction

    """
    S11 = 0
    S22 = 1
    S12 = 0

    return S11, S22, S12

def StartPicture(limit, zcenter, l1, l2, Ntot):
    """
    Initialises a plot of the initial geometry/ setup of inclusions,
    which have been specified in the main function.

    Parameters
    -------
    limit : float
        The x and y limits of the plot
    zcenter : array
        The locations of the center of the inclusions
    l1 : array
        Major axis lengths of each inclusion
    l2 : array
        Minor axis lengths of each inclusion
    Ntot : int
        The total number of inclusions in the matrix.

    """
    fig, ax = plt.subplots(figsize=(6,6))
    plt.ylim(-limit, limit)
    plt.xlim(-limit, limit)
    for i in range(Ntot):
        ell = Ellipse((zcenter[i],0),2*l1[i],2*l2[i], linewidth=2, fill=False)
        ax.add_patch(ell)
    plt.show()

def MakeXi2(limit, spacing, l0, d):
    """
    Constructing individual arrays to calculate for each inclusion, so that the
    stresses can be summed directly.

    This creates a picture ranging from 'limit' to the left of the left inclusion,
    to 'limit' from the right of the right inclusion.

    Since this isn't necessary in most future instances (unnecessarily expensive to
    look at everything if you are only looking at one crack and how it'll propagate from
    there), this code is specific to an x-displacement of l0 of the two cracks from
    one another (i.e. no rotations or y-translations); with l1 = 1 for both. However
    there is nothing in principle stopping me from adding y-translations too; rotations
    are trickier because of the grid though.

    Parameters
    -------
    limit : float
        The x and y limits of the plot
    spacing : integer
        The number of points in each direction (hence the fineness of the mesh to be constructed)
    l0 : float
        The x-distance between the center of the inclusions
    d : array
        The inter-foci distance of the respective inclusions

    Returns
    -------
    X,Y : arrays
        The mesh for the overall picture with zero centered between the inclusions
    X0,Y0 : arrays
        The mesh for the overall picture with zero centered on the 0th inclusion
    X1,Y1 : arrays
        The mesh for the overall picture with zero centered on the 1st inclusion
    zq : array
        Nested array containing the z mesh over which calculations are done for each inclusion
    Xi : array
        Nested array containing the mapped mesh (from the z-values) for each inclusion
    size : float
        The half-size of the overall picture

    """
    # global meshgrid so that the final plot is centered in the middle of the cracks
    size = limit + 1 + 0.5*l0
    x = np.linspace(-size,size,spacing)
    y = np.linspace(-size,size,spacing)
    X, Y = np.meshgrid(x, y)

    # inclusion 0
    x = np.linspace(-limit, limit + 2 + l0,spacing)
    y = np.linspace(-size,size,spacing)
    X0, Y0 = np.meshgrid(x, y)
    def z(x,y):
        return x + 1j * y
    zq0 = z(X0, Y0)

    # inclusion 1
    x = np.linspace(-(limit + 2 + l0),limit,spacing)
    y = np.linspace(-size,size,spacing)
    X1, Y1 = np.meshgrid(x, y)
    zq1 = z(X1, Y1)

    xi0 = np.arccosh(zq0/d[0])
    xi1 = np.arccosh(zq1/d[1])

    Xi = []
    Xi.append(xi0)
    Xi.append(xi1)
    Xi = np.asarray(Xi)

    zq = []
    zq.append(zq0)
    zq.append(zq1)
    zq = np.asarray(zq)

    return X, Y, X0, Y0, X1, Y1, Xi, zq, size


def bmn(m,n,dp,dq,v0p,v0q,Mu,Eta,nmax,mmax,zpq,theta):
    """
    Returns the coefficients for one pq pair, for bmnpq: bnq = sum of (Amp * bmnpqa) + (Bmp * bmnpqb).
    This is done for each m and n up to n-1 (because of indexing limits).


    Parameters
    -------
    nmax, mmax : integers
        Accuracy as set in the main function
    m,n : arrays
        Integer arrays up to nmax and mmax to loop/ broadcast over
    dp,dq : float
        The inter-foci distance of each inclusion under consideration
    v0p,v0q : float
        The v0 = exp(Z0) for each inclusion under consideration
    Mu, Eta : arrays
        The expansion coefficients for one pair: i.e. Eta[h] or Mu[h] (see
        function bmnpq) - just Eta/Mu for one pq, not the entire nested Eta/Mu array.
    zpq : array
        The distance between the centers of each pq pair (zq - zp)
    theta : array (eventually)
        Angle of rotation in degrees of each inclusion


    Returns
    -------
    bmnpqa : array
        The multiplier for Amp for calculating the bmnpq coefficient
    bmnpqb : array
        The multiplier for Bmp for calculating the bmnpq coefficient

    """
    bmnpqb = Eta[:,:-1]
    bmnpqa = []
    for i, m_value in enumerate(m):
        for w, n_value in enumerate(n[:-1]):        # only up to n-1 so that i can index n+1 later without it going weird
            # limiting range of k so that the index is within bounds
            kmax = int(0.5*(mmax-m_value))
            if 0.5*(nmax-n_value-1) < 0:        # slightly crude way for now of solving the problem where int(-.5=0)
                k = np.asarray([])
            else:
                k = np.arange(0, kmax+1, 1)

            bmnpqa_1 = Mu[i][w+1] * -0.5*dp * n_value/(n_value+1) * (v0p - 1/v0p)**2
            bmnpqa_2 = (m_value*(1 - v0q**(-2)) - n_value*(1-v0p**(-2))) * Eta[i][w]      # trying with negative n and not... not much diff
            bmnpqa_3 = (zpq.conjugate()*np.exp(1j*theta) - zpq*np.exp(-1j*theta)) * np.exp(1j*theta)  # need to change the thetas
            # bmnpqa_4 = Mu[i-1][w] + Mu[i+1][w]    # code this later but it's zero when theta is zero; need to edit m range also

            # find the sum over k for each mn
            sum_k = []
            for u, k_value in enumerate(k):
                """k_terms: each k term that needs to be in the summation
                +n instead of -n because n<0
                Eta[m][n] is indexed as Eta[m-1][n-1] - eg Eta11 = Eta[0][0],
                hence must add a -1 on the index if indexing by value"""
                k_terms = Eta[2*k_value+m_value -1][w] * (2*k_value + m_value)
                sum_k.append(k_terms)
            sumk = np.sum(sum_k)                  # k sum term
            bmnpqa_terms = bmnpqa_1 + bmnpqa_2 + sumk*(v0q - 1/v0q)**2 + bmnpqa_3
            bmnpqa.append(bmnpqa_terms)

    bmnpqa = np.asarray(bmnpqa).reshape(len(m),len(n)-1)
    return bmnpqa, bmnpqb

def bmnpq(m,n,nmax,mmax,pq,d,v0,Eta,Mu,zpq,theta):
    """
    Loops to find bmn for each pq (see function bmn for more details on bmn).

    Parameters
    -------
    nmax, mmax : integers
        Accuracy as set in the main function
    m,n : arrays
        Integer arrays up to nmax and mmax to loop/ broadcast over
    pq : array
        Permuted pairs of inclusions
    d : array
        The inter-foci distance of the respective inclusions
    v0 : array
        The array of v0 = exp(Z0) for the respective inclusions
    Mu, Eta : arrays
        The expansion coefficients for all pairs (in the order of permutations in pq)
    zpq : array
        The distance between the centers of each pq pair (zq - zp)
    theta : array (eventually)
        Angle of rotation in degrees of each inclusion


    Returns
    -------
    bmnpqa : array
        The multiplier for Amp for calculating the bmnpq coefficient
    bmnpqb : array
        The multiplier for Bmp for calculating the bmnpq coefficient

    """
    bmnpqa, bmnpqb = [], []
    for h, pair in enumerate(pq):
        dp, dq = d[pair[0]], d[pair[1]]
        v0p, v0q = v0[pair[0]], v0[pair[1]]
        bmna, bmnb = bmn(m,n,dp,dq,v0p,v0q,Mu[h],Eta[h],nmax,mmax,zpq[h],theta) # will have more thetas eventually!
        bmnpqa.append(bmna)
        bmnpqb.append(bmnb)
    bmnpqa = np.asarray(bmnpqa)
    bmnpqb = np.asarray(bmnpqb)
    return bmnpqa, bmnpqb

def M12(dim, pq, n, v0, k0, k1, omega, Z0, Ntot):
    """
    Define matrix (M1) for coefficients An, Bn, Cn, Dn;
    and their conjugates (M2). M1 and M2 are for the LHS of the equations.

    Parameters
    -------
    dim : integer
        Size of the matrix (4 * Ntot * n), where n = nmax-1 because of indexing
    pq : array
        Permuted pairs of inclusions
    n : arrays
        Integer array up to nmax to loop/ broadcast over
    v0 : array
        The array of v0 = exp(Z0) for the respective inclusions
    k0 : float
        Kappa for the matrix
    k1 : array
        Kappa for each of the N inclusions
    omega : array
        Ratio of inclusion to matrix shear moduli G1/G0 for each inclusion
    Z0 : array
        Zeta0 for each inclsion, defined by geometry of the ellipse
    Ntot : integer
        Total number of inclusions in the system

    Returns
    -------
    M1 : array
        Matrix for ABCD for LHS of equations
    M2 : array
        Matrix for (ABCD.conjugate) for LHS of equations

    """
    M1 = np.zeros((dim,dim))
    M2 = np.zeros((dim,dim))
    for i, pair in enumerate(pq):
        q_i = pair[1]
        for w, n_value in enumerate(n[:-1]):
            v0q2 = v0[q_i]**(2*n_value)
            row = q_i*4+(4*Ntot)*w
            col = q_i*4+(4*Ntot)*w
            M1[row][col] = k0
            M1[row][col+2] = -k1[q_i]
            M1[row+1][col+2] = k1[q_i] * v0q2
            M1[row+2][col] = 1
            M1[row+2][col+2] = -omega[q_i]
            M1[row+3][col+2] = -omega[q_i] * v0q2

            M2[row,col+2] = -2 * n_value * v0q2 * np.sinh(2*Z0[q_i])
            M2[row+2,col+2] = 2 * n_value * omega[q_i] * v0q2 * np.sinh(2*Z0[q_i])
            M2[row,col+3] = v0q2
            M2[row+1,col+1] = 1
            M2[row+1,col+3] = -1
            M2[row+2,col+3] = -omega[q_i] * v0q2
            M2[row+3,col+3] = -omega[q_i]
            M2[row+3,col+1] = 1
    return M1, M2

def BCcoeffs(dim, pq, n, k0, v0, Ntot):
    """
    Coefficients for boundary conditions to multiply the expansion of ABCD on the RHS with;
    i.e. to multiply with M3/4 after adding the farfield boundary conditions to them.

    Parameters
    -------
    dim : integer
        Size of the matrix (4 * Ntot * n), where n = nmax-1 because of indexing
    pq : array
        Permuted pairs of inclusions
    n : arrays
        Integer array up to nmax to loop/ broadcast over
    k0 : float
        Kappa for the matrix
    v0 : array
        The array of v0 = exp(Z0) for the respective inclusions
    Ntot : integer
        Total number of inclusions in the system

    Returns
    -------
    coeffs : array
        Matrix of coefficients on the RHS of the equations

    """
    coeffs = np.zeros((dim,dim), dtype=complex)
    for i, pair in enumerate(pq):
        p_i = pair[0]
        q_i = pair[1]
        for w, n_value in enumerate(n[:-1]):
            v0q2 = v0[q_i]**(2*n_value)
            col = q_i*4+(4*Ntot)*w
            row = q_i*4+(4*Ntot)*w
            coeffs[row][col] = -k0
            coeffs[row][col+3] = v0q2
            coeffs[row+1][col+1] = -1
            coeffs[row+1][col+2] = k0 * v0q2
            coeffs[row+2][col] = -1
            coeffs[row+2][col+3] = -v0q2
            coeffs[row+3][col+1] = -1
            coeffs[row+3][col+2] = -v0q2
    return coeffs

def M34(dim, pq, n, v0, Ntot, Eta, bmnpqa, bmnpqb):
    """
    Construct the overall matrices M3 and M4 for the boundary conditions due to other inclusions.
    Non-farfield BCs = coeffs . ( M3.(ABCD)n + M4.conj(ABCD)n )

    Note that for Amp/Anq etc, m and n are used as dummy integers. The extra m terms in the array are
    not used here (they were used for accuracy in calculating the ksum in bmnpqa).

    Parameters
    -------
    dim : integer
        Size of the matrix (4 * Ntot * n), where n = nmax-1 because of indexing
    pq : array
        Permuted pairs of inclusions
    n : arrays
        Integer array up to nmax to loop/ broadcast over
    v0 : array
        The array of v0 = exp(Z0) for the respective inclusions
    Ntot : integer
        Total number of inclusions in the system
    Eta : array
        The expansion coefficients for all pairs (in the order of permutations in pq)
    bmnpqa : array
        The multiplier for Amp for calculating the bmnpq coefficient
    bmnpqb : array
        The multiplier for Bmp for calculating the bmnpq coefficient

    Returns
    -------
    M3 : array
        Matrix for ABCD for RHS of equations - boundary condition due to other inclusions
    M4 : array
        Matrix for (ABCD.conjugate) for RHS of equations  - boundary condition due to other inclusions

    """
    M3 = np.zeros((dim,dim), dtype=complex)
    M4 = np.zeros((dim,dim), dtype=complex)
    for i, pair in enumerate(pq):
        p_i = pair[0]
        q_i = pair[1]
        for v, m_value in enumerate(n[:-1]):
            for w, n_value in enumerate(n[:-1]):
                v0q2 = v0[q_i]**2           # NOT ^(2*n)
                # print(n_value, v0q2)
                row = q_i*4+(4*Ntot)*v
                col = p_i*4+(4*Ntot)*w
                M3[row][col] = M3[row+2][col] = Eta[i][v][w]
                M4[row+1][col] = bmnpqa.conjugate()[i][v][w]
                M4[row+1][col+1] = M4[row+3][col+1] = bmnpqb.conjugate()[i][v][w]
                M4[row+3][col] = bmnpqa.conjugate()[i][v][w] - m_value * (v0q2 - 1/v0q2) * Eta[i][v][w]
    return M3, M4

def SolveCoeffs(rhs, M1, M2):
    """
    Solve for An, Bn, Cn, Dn.
    """
    # Find real part
    rhs_real = rhs.real
    lhs_real = M1 + M2
    real = np.linalg.solve(lhs_real, rhs_real)

    # Find imaginary part
    rhs_im = rhs.imag
    lhs_im = M1 - M2
    imag = np.linalg.solve(lhs_im, rhs_im)

    coeffs = 1j*imag; coeffs += real

    return coeffs

def abnpq(pq, n, Amp, Bmp, Eta, bmnpqa, bmnpqb):
    """
    Returns anpq and bnpq, with rows giving different values of n,
    and columns corresponding to the pairs of pq.
    """
    anpq = []
    bnpq = []
    for i, pair in enumerate(pq):
        p_i = pair[0]
        an = []
        bn = []
        for v, m_value in enumerate(n[:-1]):
            amn = []
            bmn = []
            for w, n_value in enumerate(n[:-1]):
                # print(m_value, n_value)
                amn.append(Amp[w][p_i]*Eta[i][v][w])
                bmn.append(Bmp[w][p_i]*bmnpqb[i][v][w] + Amp[w][p_i]*bmnpqa[i][v][w])
            an.append(np.sum(np.asarray(amn)))
            bn.append(np.sum(np.asarray(bmn)))
        anpq.append(an)
        bnpq.append(bn)
    anpq = np.transpose(np.asarray(anpq))
    bnpq = np.transpose(np.asarray(bnpq))
    return anpq, bnpq

def b_npq(pq, Z0, n1, anpq, bnpq):
    """
    Defines bnpqminus; n1 is an array of nmax-1 values for array broadcasting purposes.
    """
    sinhZ0 = []
    for i, pair in enumerate(pq):
        q_i = pair[1]
        sinhZ0.append(np.sinh(2*Z0[q_i]))
    sinhZ0 = np.asarray(sinhZ0)
    bnpqminus = bnpq - sinhZ0 * 2 * n1 * anpq
    return bnpqminus

def Potentials(v, n1, Amp, Bmp, v0, Z0, Xi, pq, anpq, bnpq, bnpqminus, a1q, b1q, b1q_minus):
    """
    Finds complex potentials for each pair pq/ each q inclusion.

    v   :   np.exp(Xi)
    n1  :   list of n values used in calculating anpq and bnpq (ultimately runs up to nmax-2 bc of indexing)
    Amp :   expansion coefficients for each mp; rows correspond to n/m, columns correspond to inclusions
    v0  :   nested array, where v0[q] gives v0 for each qth inclusion
    Z0  :   nested array, where Z0[q] gives Z0 for each qth inclusion
    Xi  :   nested array containing xi[q] for each inclusion (Xi[0] is for q=0-th inclusion)
    pq  :   permutation pairs of all inclusions
    anpq:   expansion coefficients for expanding pth potentials into the qth coordinate basis; anpq[0] is for the pq[0] pair
    a1q :   farfield anq, but only n= +-1 is non-zero

    PhiQs:
        the singular part of the potential phi in the matrix due to inclusion Q
    PsiQs:
        the singular part of the potential psi in the matrix due to inclusion Q
    PhiPQr:
        expanding phi for the pth inclusion into the qth coordinate basis for each pair pq
    PsiPQr:
        expanding psi for the pth inclusion into the qth coordinate basis for each pair pq
    PhiFarQ:
        phi for the qth inclusion due to the far field
    PsiFarQ:
        psi for the qth inclusion due to the far field

    """

    vn = v ** n1[:, np.newaxis, np.newaxis]
    v_n = v ** (-n1[:, np.newaxis, np.newaxis])
    Av_n = Amp[:,:, np.newaxis, np.newaxis] * v_n
    Bv_n = Bmp[:,:, np.newaxis, np.newaxis] * v_n

    vv0q = v/v0[:, np.newaxis, np.newaxis] - v0[:, np.newaxis, np.newaxis]/v
    sinhzx = np.sinh(Z0[:, np.newaxis, np.newaxis])/np.sinh(Xi)
    sinhzx2 = np.sinh(Xi - Z0[:, np.newaxis, np.newaxis]) * sinhzx
    sv = sinhzx * vv0q
    nAv_n = n1[:, np.newaxis, np.newaxis] * Av_n

    PhiQs = np.sum(Av_n, axis=0)
    PsiQs = np.sum(Bv_n, axis=0) - np.sum(sv * nAv_n, axis=0)

    # nxi = Xi*n1[:, np.newaxis, np.newaxis]        # vn + v_n = 2cosh(nxi)
    PhiPQr = []
    PsiPQr = []
    for i, pair in enumerate(pq):
        q_i = pair[1]       # need to select the right vq for whichever is the qth inclusion in each pair
        anpqvq = anpq[:,i, np.newaxis, np.newaxis] * (0*vn[:,q_i] + v_n[:,q_i])
        bnpqvq = bnpq[:,i, np.newaxis, np.newaxis] * v_n[:,q_i] + bnpqminus[:,i, np.newaxis, np.newaxis] * 0*vn[:,q_i]
        psi0 = np.sum(bnpqvq, axis=0)
        anpq_vq = anpq[:,i, np.newaxis, np.newaxis] * (v_n[:,q_i] - 0*vn[:,q_i])
        psi1 = np.sum(2 * n1[:, np.newaxis] * anpq_vq * sinhzx2[q_i], axis=0)
        PhiPQr.append(np.sum(anpqvq, axis=0))
        PsiPQr.append(psi0-psi1)
    PhiPQr = np.asarray(PhiPQr)
    PsiPQr = np.asarray(PsiPQr)

    # since all but n=1 are zero,
    PhiFarQ = a1q[:, np.newaxis, np.newaxis] * (v+1/v)
    PsiFarQ = ((b1q[:, np.newaxis, np.newaxis] - 2 * sinhzx2 * a1q[:, np.newaxis, np.newaxis]) * 1/v
                + (b1q_minus[:, np.newaxis, np.newaxis] + 2 * sinhzx2 * a1q[:, np.newaxis, np.newaxis]) * v)

    return PhiQs, PsiQs, PhiPQr, PsiPQr, PhiFarQ, PsiFarQ

def Pot0(Ntot, pq, PhiPQr, PsiPQr, PhiQs, PsiQs):
    """
    Generate Phi0 and Psi0. Pot0 = PotQs + Sum(PotPQr[p,q], for p up to Ntot)
    """
    SumPhiQr = []
    SumPsiQr = []
    for j in range(Ntot):
        # creating masked arrays, for each inclusion (number j)
        MaskQPhiPQr = np.ma.array(PhiPQr, mask=True)
        MaskQPsiPQr = np.ma.array(PsiPQr, mask=True)
        for i, pair in enumerate(pq):
            q_i = pair[1]       # the inclusion being looked at in this pair
            if q_i == j:
                # unmasking the parts of the array relevant for summation,
                # i.e. when q_i coincides with the inclusion currently being looked at
                MaskQPhiPQr.mask[i] = False
                MaskQPsiPQr.mask[i] = False
        SumPhiQr.append(np.sum(MaskQPhiPQr, axis=0))
        SumPsiQr.append(np.sum(MaskQPsiPQr, axis=0))
    SumPhiQr = np.asarray(SumPhiQr)
    SumPsiQr = np.asarray(SumPsiQr)

    Phi0 = PhiQs + SumPhiQr
    Psi0 = PsiQs + SumPsiQr

    return Phi0, Psi0

def dz(potential, stepsize):
    """
    Calculate the derivative wrt z where z = x+iy.
    """
    dy, dx = np.gradient(potential,stepsize)
    dz = dx - 1j*dy
    return dz

def plotc(X, Y, fn, name, levels, l1, l2):
    """
    Make a contour plot of stresses, at specified contours, without edges.
    """
    fig, ax = plt.subplots(figsize=(6,6))
    # Contour fill plot
    contour = plt.contourf(X[1:-1,1:-1],Y[1:-1,1:-1],fn[1:-1,1:-1], levels, cmap=cm.coolwarm,zorder=1)
    # Contour line plot
    plt.contour(X[1:-1,1:-1],Y[1:-1,1:-1],fn[1:-1,1:-1], levels, cmap=cm.bwr,zorder=2,alpha=0.6)
    plt.axis('equal')
    plt.colorbar(contour,fraction=0.046, pad=0.04)
    plt.title("{}".format(name))
    # Add ellipse
    ell = Ellipse((0,0),2*l1,2*l2, linewidth=1, fill=True, facecolor='w', edgecolor='k',zorder=10)
    ax.add_patch(ell)
    plt.show()

def plot2(X, Y, fn, name, levels, zcenter, l1, l2):
    """
    Make a contour plot of stresses including both inclusions, at specified contours, without edges.
    """
    fig, ax = plt.subplots(figsize=(6,6))
    # Contour fill plot
    contour = plt.contourf(X[1:-1,1:-1],Y[1:-1,1:-1],fn[1:-1,1:-1], levels, cmap=cm.coolwarm,zorder=1)
    # Contour line plot
    plt.contour(X[1:-1,1:-1],Y[1:-1,1:-1],fn[1:-1,1:-1], levels, cmap=cm.bwr,zorder=2,alpha=0.6)
    plt.axis('equal')
    plt.colorbar(contour,fraction=0.046, pad=0.04)
    plt.title("{}".format(name))
    # Add ellipse
    ell = Ellipse((zcenter[0].real,0),2*l1[0],2*l2[0], linewidth=1, fill=True, facecolor='w', edgecolor='k',zorder=10)
    ax.add_patch(ell)
    ell2 = Ellipse((zcenter[1].real,0),2*l1[1],2*l2[1], linewidth=1, fill=True, facecolor='w', edgecolor='k',zorder=10)
    ax.add_patch(ell2)
    plt.show()



def main():
 #  ___ _      _ _           _
 # | __(_)_ _ (_) |_ ___    /_\  _ _ _ _ __ _ _  _
 # | _|| | ' \| |  _/ -_)  / _ \| '_| '_/ _` | || |
 # |_| |_|_||_|_|\__\___| /_/ \_\_| |_| \__,_|\_, |
 #                                            |__/
 #        __   ___         _         _
 #  ___ / _| |_ _|_ _  __| |_  _ __(_)___ _ _  ___
 # / _ \  _|  | || ' \/ _| | || (_-< / _ \ ' \(_-<
 # \___/_|   |___|_||_\__|_|\_,_/__/_\___/_||_/__/

    """
    Initialise problem geometry
    """
    # Set up problem geometry
    N1 = 2                              # number of inclusions in the x direction
    N2 = 1                              # number of inclusions in the y direction
    Ntot = N1*N2                        # total number of inclusions
    S11, S22, S12 = FarFieldStress()    # far field stress
    l1 = np.full(Ntot, 1)               # major axis length
    l2 = np.asarray([0.4,0.2])
    l0 = 1                              # distance apart the edges of the ellipses are
    zcenter = np.array([-(0.5*l0+1)+0j, (0.5*l0+1)+0j])
    e = l2/l1                           # aspect ratio
    d = np.sqrt(l1*l1 - l2*l2)          # inter-foci distance

    # Create list of permuted pairs of inclusions (subsequent pq interaction terms use this order)
    permute = np.arange(0,Ntot,1)       # starting from 0 so easier to index
    pq = list(permutations(permute,2))  # all pairs of pq

    """
    For plotting the combined stress field, it is convenient for this setup to
    define the arrays (on which the stresses are calculated) individually, so that
    to superimpose the stresses, the arrays can just be added without indexing the
    relative displacements.
    """

    spacing = 200                       # fineness of mesh in calculating potentials
    limit = 4                           # boundaries of each local coordinate system
    theta = 0                           # should eventually be an array

    # Visualising starting geometry
    # StartPicture(4, zcenter, l1, l2, Ntot)

    Combine = True

    if Combine == False:
    # Creating Xi for each inclusion (xi = zeta + i*eta)
        x = np.linspace(-limit, limit,spacing)
        y = np.linspace(-limit,limit,spacing)
        X, Y = np.meshgrid(x, y)
        def z(x,y):
            return x + 1j * y
        zq = z(X, Y)
        Xi = np.arccosh(zq[np.newaxis, :]/d[:,np.newaxis, np.newaxis])
        h = 2*2*limit/(spacing)

    else:
        X, Y, X0, Y0, X1, Y1, Xi, zq, size = MakeXi2(limit, spacing, l0, d)
        h = 2*2*size/(spacing)


    """
    Initialise physical constants and specify accuracy
    """

    # Define physical constants
    nu0, G0, k0, nu1, G1, k1 = PhysConsts(Ntot)
    omega = G1/G0                       # defined for eventual convenience in calculations

    # Set accuracy
    # -for basis functions
    nmax = 2                            # (IMPT!! the actual n accuracy is to nmax-1 because of indexing requirements later)
    # -for expansion coefficients
    m_accuracy = 10
    mmax =  2 * m_accuracy + nmax
    lmax = 10
    jmax = 5
    # Create arrays that can be looped over/ broadcasted
    n = np.arange(1, nmax+1, 1)
    m = np.arange(1, mmax+1, 1)
    l = np.arange(0, lmax+1, 1)
    j = np.arange(0, jmax+1, 1)

    # Find geometry specific terms
    Z0 = 0.5 * np.log((1+e)/(1-e))
    v0 = np.exp(Z0)
    zpq, dpq, dpdq = [], [], []
    for pair in pq:
        p = pair[0]
        q = pair[1]
        zpq.append(zcenter[q] - zcenter[p])
        dpq.append(d[q]+d[p])
        dpdq.append(d[p]/d[q])
    zpq, dpq, dpdq = np.asarray(zpq), np.asarray(dpq), np.asarray(dpdq)
    zdpq = zpq/dpq
    vpq = zdpq + np.sqrt(zdpq**2 - 1)*np.sign(zdpq)         # to take the +/- square root as needed
    vpqprime = (1/dpq)*(1+ zdpq / (np.sqrt((zdpq)**2 - 1)*np.sign(zdpq)))


    """
    Expansion Coefficients
    """

    Near = True             # calculates Eta/Mu_Near if true and Eta/Mu_Far if False
    Eta, Mu = ExpansionCoeffs(Near,m,n,l,j,zpq,dpq,dpdq,vpq,vpqprime,pq,Ntot,d)

    """
    Matrix
    """

    # Find coefficients for Amp, Bmp
    bmnpqa, bmnpqb = bmnpq(m,n,nmax,mmax,pq,d,v0,Eta,Mu,zpq,theta)
    # Dimension of the matrix
    dim = 4 * Ntot * (nmax-1)
    # Defining matrix for the (non-Amp) coefficients of the boundary conditions (RHS of the equations)
    coeffs = BCcoeffs(dim, pq, n, k0, v0, Ntot)
    # Matrix for the LHS of the equations
    M1, M2 = M12(dim, pq, n, v0, k0, k1, omega, Z0, Ntot)
    # Matrix for assembling the A/Bmp expansion of abmnpq coefficients (RHS of the equations)
    M3, M4 = M34(dim, pq, n, v0, Ntot, Eta, bmnpqa, bmnpqb)
    # Assembled RHS of the equations
    M3_2 = np.dot(coeffs, M3)
    M4_2 = np.dot(coeffs, M4)
    # Creating final matrices that can be solved for x in the form of Mx = b.
    M5 = M1 - M3_2
    M6 = M2 - M4_2


    """
    Farfield Boundary Conditions

    All anq/bnq is zero except for n=1, so there are q (i.e. Ntot) non-zero terms.
    ab = [anq, bnq.conjugate(), a-nq, b-nq.conjugate()]
    """

    ab = np.zeros((dim,1),dtype=complex)
    a1q = (S11 + S22)/(8*G0) * d/2
    b1q_minus = a1q/(v0**2) + d/(8*G0) * (S22 - S11 + 2j*S12)
    b1q = b1q_minus + 2 * a1q * np.sinh(2*Z0)
    for i, pair in enumerate(pq):
        q_i = pair[1]
        ab[q_i*4] = ab[q_i*4+2] = a1q[q_i]
        ab[q_i*4+1] = b1q.conjugate()[q_i]
        ab[q_i*4+3] = b1q_minus.conjugate()[q_i]

    bcbar = np.dot(coeffs,ab)


    """
    Solve for [ABCD]mp
    """
    ABCD = SolveCoeffs(bcbar, M5, M6)

    # Rows correspond to n values, and columns correspond to inclusions by index.
    Amp = ABCD[::4].reshape(nmax-1, Ntot)
    Bmp = ABCD[1::4].reshape(nmax-1, Ntot)
    Cmp = ABCD[2::4].reshape(nmax-1, Ntot)
    Dmp = ABCD[3::4].reshape(nmax-1, Ntot)

    """
    Potentials
    """
    v = np.exp(Xi)
    n1 = n[:-1, np.newaxis]

    anpq, bnpq = abnpq(pq, n, Amp, Bmp, Eta, bmnpqa, bmnpqb)
    bnpqminus = b_npq(pq, Z0, n1, anpq, bnpq)

    PhiQs, PsiQs, PhiPQr, PsiPQr, PhiFarQ, PsiFarQ = Potentials(v, n1, Amp, Bmp, v0, Z0, Xi, pq, anpq, bnpq, bnpqminus, a1q, b1q, b1q_minus)
    Phi0, Psi0 = Pot0(Ntot, pq, PhiPQr, PsiPQr, PhiQs, PsiQs)
    Phi = Phi0 + PhiFarQ
    Psi = Psi0 + PsiFarQ

    # Calculate derivatives of potentials
    # wprime = d[:, np.newaxis, np.newaxis] * np.sinh(Xi)
    # h = 2*2*limit/(spacing)
    dPsi, dPhi, d2Phi = [], [], []
    for i in range(Ntot):
        dpsi = dz(Psi[i], h)
        dphi = dz(Phi[i], h)
        ddphi = dz(dphi, h)
        dPsi.append(dpsi)
        dPhi.append(dphi)
        d2Phi.append(ddphi)
    dPsi = np.asarray(dPsi)
    dPhi = np.asarray(dPhi)
    d2Phi = np.asarray(d2Phi)

    """
    Stresses
    """

    sum = 4 * G0 * (dPhi + dPhi.conjugate()).real
    rhs = 4 * G0 * ((zq.conjugate()-zq) * d2Phi - dPhi + dPsi)
    diff = rhs.real
    sigma12 = 0.5 * rhs.imag
    sigma22 = (0.5 * (sum+diff))
    sigma11 = (0.5 * (sum-diff))

    lim = 0.2
    levels = np.linspace(-lim, lim, 27)
    middle = int(spacing*0.5)

    if Combine == True:
        # Superimposing the stresses (only if original arrays were defined with the right relative displacement!)
        s12tot = (sigma12[0]+sigma12[1])*0.5
        s11tot = (sigma11[0]+sigma11[1])*0.5
        s22tot = (sigma22[0]+sigma22[1])*0.5

        crop = int(0.5*spacing) + int((0.5*l0 + 1 - limit)*spacing/(2*size))
        crop2 = int(0.5*spacing) - int(limit * spacing/(2*size))

        """
        contour plots for s12, s11, s22 for:
        - individual stresses in overall picture
        - total stresses in overall picture
        - total stresses in cropped picture for each individual inclusion
        """

        # s12
        # plotc(X0,Y0,sigma12[0], "sigma12", levels, l1[0], l2[0])
        # plotc(X1,Y1,sigma12[1], "sigma12", levels, l1[1], l2[1])
        plot2(X,Y,s12tot,"sigma12",levels,zcenter,l1,l2)
        # plotc(X0[crop2:-crop2,:-crop],Y0[crop2:-crop2,:-crop],s12tot[crop2:-crop2,:-crop],"s12 left",levels,l1[0],l2[0])
        # plotc(X1[crop2:-crop2,crop:],Y1[crop2:-crop2,crop:],s12tot[crop2:-crop2,crop:],"s12 right",levels,l1[1],l2[1])

        # s11
        # plotc(X0,Y0,sigma11[0], "sigma11", levels, l1[0], l2[0])
        # plotc(X1,Y1,sigma11[1], "sigma11", levels, l1[1], l2[1])
        # plot2(X,Y,s11tot,"s11",levels,zcenter,l1,l2)
        # plotc(X0[crop2:-crop2,:-crop],Y0[crop2:-crop2,:-crop],s11tot[crop2:-crop2,:-crop],"s11 left",levels,l1[0],l2[0])
        # plotc(X1[crop2:-crop2,crop:],Y1[crop2:-crop2,crop:],s11tot[crop2:-crop2,crop:],"s11 right",levels,l1[1],l2[1])

        # s22
        # plotc(X0,Y0,sigma22[0], "sigma22", levels, l1[0], l2[0])
        # plotc(X1,Y1,sigma22[1], "sigma22", levels, l1[1], l2[1])
        # plot2(X,Y,s22tot,"s22",levels,zcenter,l1,l2)
        # plotc(X0[crop2:-crop2,:-crop],Y0[crop2:-crop2,:-crop],s22tot[crop2:-crop2,:-crop],"s22 left",levels,l1[0],l2[0])
        # plotc(X1[crop2:-crop2,crop:],Y1[crop2:-crop2,crop:],s22tot[crop2:-crop2,crop:],"s22 right",levels,l1[1],l2[1])

        "line plots"

        # x = np.linspace(-size, size, spacing)
        # plt.scatter(x,s12tot[middle],marker='.', label='s12')
        # plt.scatter(x,s11tot[middle],marker='.', label='s11')
        # plt.scatter(x,s22tot[middle],marker='.', label='s22')
        # plt.title("Stresses along the center line")
        # plt.legend()
        # plt.show()

    else:
        "contour plots (uncombined stresses, individual inclusions)"

        # plotc(X,Y,sigma12[0], "sigma12", levels, l1[0], l2[0])
        # plotc(X,Y,sigma12[1], "sigma12", levels, l1[1], l2[1])

        plotc(X,Y,sigma11[0], "sigma11", levels, l1[0], l2[0])
        plotc(X,Y,sigma11[1], "sigma11", levels, l1[1], l2[1])

        # plotc(X,Y,sigma22[0], "sigma22", levels, l1[0], l2[0])
        # plotc(X,Y,sigma22[1], "sigma22", levels, l1[1], l2[1])

        "line plots"

        x = np.linspace(-limit, limit, spacing)

        # inclusion 0
        # plt.scatter(x,sigma12[0][middle],marker='.', label='s12')
        # plt.scatter(x,sigma11[0][middle],marker='.', label='s11')
        # plt.scatter(x,sigma22[0][middle],marker='.', label='s22')
        # plt.title("inclusion 0")
        # plt.legend()
        # plt.show()

        # inclusion 1
        # plt.scatter(x,sigma12[1][middle],marker='.', label='s12')
        # plt.scatter(x,sigma11[1][middle],marker='.', label='s11')
        # plt.scatter(x,sigma22[1][middle],marker='.', label='s22')
        # plt.title("inclusion 1")
        # plt.legend()
        # plt.show()






if __name__ == '__main__':
    main()
