#!/usr/bin/env python3

"Solving for the stress fields of multiple ellipses."

from itertools import permutations
from scipy.special import factorial
from matplotlib.patches import Ellipse
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import time
# other .py files:
from setup import PhysConsts, FarFieldStress, StartPicture, MakeXi, MakeXiE
from EtaMu import ExpansionCoeffs
from matrix import bmnpq, BCcoeffs, M12, M34, SolveCoeffs
from potentials import abnpq, b_npq, Potentials, Pot0
from stresses import dPot, Stresses
from plot import plotc, plot2, PerimeterStress
from sif import SIF
from resstress import RStress


def main():
 #  ___ _      _ _           _
 # | __(_)_ _ (_) |_ ___    /_\  _ _ _ _ __ _ _  _
 # | _|| | ' \| |  _/ -_)  / _ \| '_| '_/ _` | || |
 # |_| |_|_||_|_|\__\___| /_/ \_\_| |_| \__,_|\_, |
 #                                            |__/
 #        __   ___         _         _
 #  ___ / _| |_ _|_ _  __| |_  _ __(_)___ _ _  ___
 # / _ \  _|  | || ' \/ _| | || (_-< / _ \ ' \(_-<
 # \___/_|   |___|_||_\__|_|\_,_/__/_\___/_||_/__/

    start = time.time()

    """
    Initialise problem geometry
    """
    # Set up problem geometry`
    Ntot = 2                            # total number of inclusions
    ang = 0                             # angle of applied stress (degrees)
    S11, S22, S12 = FarFieldStress(ang) # far field stress

    length1 = 1
    l1 = np.full(Ntot, length1)         # major axis length
    l2 = np.asarray([0.5,0.1])          # minor axis length
    l0 = 2                              # for two ellipses: x distance apart the edges of unrotated ellipses are
    zcenter = np.array([-(0.5*l0+length1)+0j, (0.5*l0+length1)+0.5j]) # positions of ellipse centers
    theta = np.asarray([15,-10])        # tilt angle in degrees
    e = l2/l1                           # aspect ratio
    thetar = theta/180 * np.pi          # tilt angle in radians
    eith = np.exp(1j*thetar)            # exp(i*theta)
    d = np.sqrt(l1*l1 - l2*l2)*eith     # inter-foci distance
    Z0 = 0.5 * np.log((1+e)/(1-e))      # zeta which defines the ellipse in elliptical coordinate space
    v0 = np.exp(Z0)
    # Define physical constants
    nu0, G0, k0, nu1, G1, k1 = PhysConsts(Ntot) # poissons ratio, shear modulus, kappa
    omega = G1/G0                       # defined for eventual convenience in calculations

    print('l0:',l0)
    print('l1:',length1)

    """
    Create coordinate systems
    """

    # Choose coordinate system and if stresses are superimposed
    EllCoords = False                   # if True, Global must = False, and elliptical coordinates are used
    Global = True                       # if True, stresses will be combined in the global coordinate system
    ResStress = True                    # if True, and Global is True in z-coords, residual stresses will be superimposed on the system

    spacing = 501                       # fineness of mesh in calculating potentials
    limit = 2                           # edge buffer (from center of edgemost ellipses) when plotting combined stresses

    # Boundaries for local coordinate systems
    Z_limx = 3                          # x limit in the z coordinate system
    Z_limy = 3                          # y limit in the z coordinate system
    E_limx = 4                          # zeta limit in multiples of Z0; (spacing-1) must be divisible by E_limx to index Z0
    E_limy = np.pi                      # eta limit

    # Creating Xi for each inclusion (xi = zeta + i*eta)
    def z(x,y):
        return x + 1j * y
    if Global == False:
        if EllCoords == False:
            # Set up the grid in z space
            x = np.linspace(-Z_limx,Z_limx,spacing)
            y = np.linspace(-Z_limy,Z_limy,spacing)
            X, Y = np.meshgrid(x, y)
            zq = z(X, Y)
            Xi = np.arccosh(zq[np.newaxis, :]/d[:,np.newaxis, np.newaxis])
            hx, hy = 4*Z_limx/spacing, 4*Z_limy/spacing
        else:
            # Set up the grid in elliptical coordinate space
            Xi, hx, hy, zq, Z0_index, xi_eta, xi_zeta = MakeXiE(z, E_limx, E_limy, spacing, Z0, d)
    else:
        # Set up individual local coordinates (for calculating) and combined grid (for plotting)
        x, y, X, Y, Xq, Yq, hx, hy, zq, Xi = MakeXi(zcenter, limit, spacing, d, z)

    # Visualising starting geometry
    # StartPicture(limit, zcenter, l1, l2, theta, Ntot)

    """
    Specify accuracy
    """

    # for basis functions
    nmax = 2                            # IMPT!! the actual n accuracy is to nmax-1 (because of indexing requirements later)
    # for expansion coefficients
    m_accuracy = 10
    mmax =  2 * m_accuracy + nmax
    lmax = 20
    jmax = 20
    # Create arrays that can be looped over/ broadcasted
    n = np.arange(1, nmax+1, 1)
    m = np.arange(1, mmax+1, 1)
    l = np.arange(0, lmax+1, 1)
    j = np.arange(0, jmax+1, 1)

    print("n:", nmax-1)

    """
    Create pair-related terms
    """

    # Create list of permuted pairs of inclusions (subsequent pq interaction terms use this order)
    permute = np.arange(0,Ntot,1)       # starting from 0 so easier to index
    pq = list(permutations(permute,2))  # all possible pairs of pq

    # Find geometric pair-specific terms
    zpq, dpq, dpdq, thetapq = [], [], [], []
    for pair in pq:
        p = pair[0]
        q = pair[1]
        zpq.append(zcenter[q] - zcenter[p])
        dpq.append(d[q]+d[p])
        dpdq.append(d[p]/d[q])
        thetapq.append(theta[q] - theta[p])
    zpq, dpq, dpdq, thetapq = np.asarray(zpq), np.asarray(dpq), np.asarray(dpdq), np.asarray(thetapq)
    zdpq = zpq/dpq
    vpq = zdpq + np.sqrt(zdpq**2 - 1)*np.sign(zdpq)         # to take the +/- square root as needed
    vpqprime = (1/dpq)*(1+ zdpq / (np.sqrt((zdpq)**2 - 1)*np.sign(zdpq)))
    eithpq = np.exp(1j*thetapq/180*np.pi)

    """
    Expansion coefficients
    """

    Near = True             # calculates Eta/Mu_Near if true and Eta/Mu_Far if False
    Eta, Mu = ExpansionCoeffs(Near,m,n,l,j,zpq,dpq,dpdq,vpq,vpqprime,pq,Ntot,d)

    """
    Creating matrices
    """

    dim = 4 * Ntot * (nmax-1)   # dimension of the matrix

    # Find coefficients for Amp, Bmp
    bmnpqa, bmnpqb, smnpq = bmnpq(m,n,nmax,mmax,pq,d,v0,Eta,Mu,zpq,eith,eithpq)
    # Defining matrix for the (non-Amp) coefficients of the boundary conditions (RHS of the equations)
    coeffs = BCcoeffs(dim, pq, n, k0, v0, Ntot)
    # Matrix for the LHS of the equations
    M1, M2 = M12(dim, pq, n, v0, k0, k1, omega, Z0, Ntot)
    # Matrix for assembling the A/Bmp expansion of abmnpq coefficients (RHS of the equations)
    M3, M4 = M34(dim, pq, n, v0, Ntot, smnpq, bmnpqa, bmnpqb)
    # Assembled RHS of the equations
    M3_2 = np.dot(coeffs, M3)
    M4_2 = np.dot(coeffs, M4)
    # Creating final matrices that can be solved for x in the form of Mx = b.
    M5 = M1 - M3_2
    M6 = M2 - M4_2

    """
    Farfield boundary conditions
    """
    # all anq/bnq are zero except for n=1, so there are Ntot non-zero terms.
    # ab = [anq, bnq.conjugate(), a-nq, b-nq.conjugate()]

    ab = np.zeros((dim,1),dtype=complex)
    a1q = (S11 + S22)/(8*G0) * d/2 * eith**(-1)
    b1q_minus = a1q/(v0**2) + d/(8*G0) * (S22 - S11 + 2j*S12) * eith
    b1q = b1q_minus + 2 * a1q * np.sinh(2*Z0)
    for i, pair in enumerate(pq):
        q_i = pair[1]
        ab[q_i*4] = ab[q_i*4+2] = a1q[q_i]
        ab[q_i*4+1] = b1q.conjugate()[q_i]
        ab[q_i*4+3] = b1q_minus.conjugate()[q_i]
    bcbar = np.dot(coeffs,ab)

    """
    Solve for [ABCD]mp
    """
    ABCD = SolveCoeffs(bcbar, M5, M6)

    # Rows correspond to n values, and columns correspond to inclusions by index.
    Amp = ABCD[::4].reshape(nmax-1, Ntot)
    Bmp = ABCD[1::4].reshape(nmax-1, Ntot)
    Cmp = ABCD[2::4].reshape(nmax-1, Ntot)
    Dmp = ABCD[3::4].reshape(nmax-1, Ntot)

    """
    Potentials
    """
    v = np.exp(Xi)
    n1 = n[:-1, np.newaxis]

    # Calculating coefficients for potentials
    anpq, bnpq = abnpq(pq, n, Amp, Bmp, smnpq, bmnpqa, bmnpqb)
    bnpqminus = b_npq(pq, Z0, n1, anpq, bnpq)

    # Calculating potentials for each inclusion
    Exp = False                 # if True, exponential terms are included (accurate near, but fields far away blow up - affects PotPQr)
    PhiQs, PsiQs, PhiPQr, PsiPQr, PhiFarQ, PsiFarQ = Potentials(Exp, v, n1, Amp, Bmp, v0, Z0, Xi, pq, anpq, bnpq, bnpqminus, a1q, b1q, b1q_minus)
    Phi0, Psi0 = Pot0(Ntot, pq, PhiPQr, PsiPQr, PhiQs, PsiQs)
    Phi = Phi0 + PhiFarQ
    Psi = Psi0 + PsiFarQ

    print('Exp:',Exp)

    # Calculate derivatives of potentials
    minuseith = eith**(-1)
    wprime = d[:, np.newaxis, np.newaxis] * np.sinh(Xi) * minuseith[:, np.newaxis, np.newaxis]
    dPsi, dPhi, d2Phi = dPot(EllCoords, Psi, Phi, hx, hy, minuseith, wprime, Ntot)

    """
    Stresses
    """

    sigma11, sigma12, sigma22 = Stresses(zq, minuseith, dPsi, dPhi, d2Phi, G0, thetar)

    calc = time.time()
    print("non-plotting time (s):",calc-start)

    """
    Residual stresses
    """

    if EllCoords == False and Global == True and ResStress == True:
        ampl = 1
        g = 1
        rStress, mask = RStress(Xq,Yq,thetar,e,l1,g,ampl)

    """
    Plotting
    """

    colourlim = 1.5
    levels = np.linspace(-colourlim, colourlim, 27)
    middle = int(spacing*0.5)

    # Residual stress
    # plot2(X,Y,rStress,"Residual stress",levels,zcenter,l1,l2,theta)

    if Global == True:
        # Superimposing the stresses
        s12tot = np.sum(sigma12, axis=0) / Ntot
        s11tot = np.sum(sigma11, axis=0) / Ntot
        s22tot = np.sum(sigma22, axis=0) / Ntot
        # Masked arrays
        mS12 = np.ma.masked_array(s12tot, mask)
        mS11 = np.ma.masked_array(s11tot, mask)
        mS22 = np.ma.masked_array(s22tot, mask)

        "Contour plots"
        # for individual stresses
        for i in range(Ntot):
            # plotc(Xq[i],Yq[i],sigma12[i], "sigma12", levels, l1[i], l2[i], theta[i])
            # plotc(Xq[i],Yq[i],sigma11[i], "sigma12", levels, l1[i], l2[i], theta[i])
            # plotc(Xq[i],Yq[i],sigma22[i], "sigma12", levels, l1[i], l2[i], theta[i])
            continue
        # for total stresses
        # plot2(X,Y,s12tot,"S12",levels,zcenter,l1,l2,theta)
        # plot2(X,Y,s11tot,"S11",levels,zcenter,l1,l2,theta)
        plot2(X,Y,s22tot,"S22",levels,zcenter,l1,l2,theta)

        "Residual stress"
        if ResStress == True:
            rS12 = rStress + mS12
            rS11 = rStress + mS11
            rS22 = rStress + mS22
            # plot2(X,Y,rS12,"S12 + Residual stress",levels,zcenter,l1,l2,theta)
            # plot2(X,Y,rS11,"S11 + Residual stress",levels,zcenter,l1,l2,theta)
            plot2(X,Y,rS22,"S22 + Residual stress",levels,zcenter,l1,l2,theta)

        "Line plots (across the middle)"
        # (this is useful when the inclusions are all on the same horizontal line,
        # so that the index 'middle' gives the stresses along that line.)

        # print('s22 at [0,0]:', s22tot[middle,middle]) # after changing my code - only valid if inclusions are symmetrical about y
        # plt.plot(x,s22tot[middle,:],marker='.', label='s22')
        # plt.title("Stresses along the center line")
        # plt.legend()
        # plt.show()

    else:
        if EllCoords == False:
            "Contour plots (uncombined stresses, individual inclusions)"
            for i in range(Ntot):
                # plotc(X,Y,sigma12[i], "sigma12", levels, l1[i], l2[i], theta[i])
                # plotc(X,Y,sigma11[i], "sigma12", levels, l1[i], l2[i], theta[i])
                plotc(X,Y,sigma22[i], "sigma22", levels, l1[i], l2[i], theta[i])
                continue
            # plotc(X,Y,sigma22[0], "sigma22", levels, l1[0], l2[0], theta[0])

            "Line plots (across the middle)"
            q = 0               # for inclusion q
            # plt.plot(x,sigma12[q][middle], label='s12')
            # plt.plot(x,sigma11[q][middle], label='s11')
            # plt.plot(x,sigma22[q][middle], label='s22')
            # plt.title("inclusion {}".format(q))
            # plt.legend()
            # plt.show()

        else:
            "Contour plots"
            q = 0               # for inclusion q
            plotc(zq.real[q],zq.imag[q],sigma12[q], "sigma12", levels, l1[q], l2[q], theta[q])
            plotc(zq.real[q],zq.imag[q],sigma11[q], "sigma11", levels, l1[q], l2[q], theta[q])
            plotc(zq.real[q],zq.imag[q],sigma22[q], "sigma22", levels, l1[q], l2[q], theta[q])

            "Line plots (of stress around the perimeter)"
            q = 0
            PerimeterStress(sigma11, sigma12, sigma22, Z0_index, xi_eta, q)

    """
    Stress intensity factors
    """
    k1, k2, k1_m, k2_m = SIF(d,eith,G0,n,Amp,Bmp)

    end = time.time()
    print('time (s):', end-start)


if __name__ == '__main__':
    main()
