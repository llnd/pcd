# Finite Array of Inclusions

This code computes the stress field in a plane containing a finite number of arbitrarily oriented elliptical inclusions. It does so using the multipole method, which is based on complex variable plane elasticity theory. The stress field around each inclusion (which takes into account its interactions with the other inclusions) can be superimposed to visualise the global stress field. 

In addition, an elliptical decaying stress can be added around each inclusion to visualise the effect of residual stress (which can arise due to e.g. thermal effects), and how that affects the field produced. 

---



## How it works

In complex variable plane elasticity, useful physical properties like stress, tractions and displacements can be related to complex potentials $\phi$ and $\psi$. These potentials are unique and hence obtaining the correct potential (which satisfies the necessary boundary conditions for the problem) solves for the stress field of that problem.

In the multipole method, $\phi$ and $\psi$ are treated as a series expansion with basis functions $e^{\pm n \xi}$ or $\overline{e^{\pm n \xi}}$ depending on the relevant boundary conditions, where $n$ is a positive integer. Increasing the number of basis functions increases the accuracy of the computation, but takes a longer time. 

The code is thus mainly concerned with computing the coefficients of the basis functions. Since the coefficients take into account the interactions between all $N$ inclusions, this ultimately involves solving $N \times n \times 4$ linear equations which satisfy the necessary boundary conditions of the problem.

Once the coefficients are computed, the potentials can be computed and the stress fields calculated as well.



### A note on coordinate systems and superposition

Because the inclusions are elliptical, most of the mathematical theory the method is based on is in elliptical coordinate space, where $\xi = \zeta + i\eta$ and relates to the cartesian coordinate system $z = x + iy$ by $z = d\ cosh(\xi)$, where $d$ is unique to the geometry of each ellipse. In elliptical coordinates, $\zeta$ is analagous to the radius of a circle in spherical coordinates, and $\eta$ is analagous to the angle $\phi$. 

As such, the method involves creating local rotated coordinate systems $y_q$ on which the potentials are calculated (rotated according to the angle the ellipse is tilted to the horizontal), before the stress is rotated back into its local coordinate system $z_q$, and translated into the global coordinate system $Z_q$. The code only explicitly does the first two steps, but achieves the third by defining the local coordinate systems in an extended grid, so that the stresses in each coordinate system are already placed correctly relative to one another and can be added without messy indexing of arrays. 

In this code, the local coordinate systems can be created in either z-space or elliptical coordinate space, depending on what is required. Note that this results in using different chain rule factors when differentiating the potentials, and that the differentiation will be carried out as $dz$ or $d\xi$ respectively, according to how the original grid is defined. The $x/y$ or $\zeta/\eta$ axes must be evenly spaced and the stepsize `h` defined according to the spacing for this to be accurate (so don't change it). 

Both coordinate systems give identical individual stress fields, but for plotting or superimposing the stress fields, z-space gives better plots and allows much easier superposition. For plotting stresses around the perimeter of an inclusion, elliptical coordinate space is much better as the $\zeta_0$ value that describes the 'radius' of the ellipse can be indexed easily (as long as the original spacing was set up correctly - see actual code for more information). Currently superposition is only possible in cartesian coordinates; though it might be possible to regrid the elliptical coordinates with `scipy.ndimage.interpolation.map_coordinates` and translate them as necessary, it might be less effort to figure out how to index the ellipse perimeter etc. in z-space.

**TLDR**; for plotting or computing overall stress fields, use cartesian coordinates; for plotting stress along the inclusion perimeter, use elliptical coordinates.



### More detail

#### Choosing n

When choosing n, first consider the geometry of the situation. A large n is necessary only when computing the field between close inclusions; in this case n=27 has been found to be sufficient. Due to the positive exponential term in some basis functions, this causes the field far away to blow up for n>1, and hence to visualise the stresses it is necessary to set the coefficients to zero if this is desired (this can be done by setting `Exp` to `False` in the code). Also note that in the code, the maximum value of n used in computing basis functions is actually `nmax-1` because of indexing requirements later in the code.



#### Expansion coefficients

These are the coefficients $\eta^{pq}_{mn}$ and $\mu^{pq}_{mn}$ that allow the basis functions of the pth inclusion to be expanded in the coordinate system of the qth inclusion (and m and n refer to the basis functions), and are used to compute the coefficients of the $4Nn$ linear equations. 

There are currently two methods coded to compute them: `EtaFarPQ` and `MuFarPQ`, or `EtaNearPQ` and `MuNearPQ`. The former is more computationally efficient but the latter is necessary to converge for close inclusions. Both these methods require sums over additional variables (`l` and `j`) and these variables determine the accuracy of the coefficients. 

At the moment it is only possible to select one method for calculating the coefficients of every pq pair (by setting `Near` to True or False as desired), but it should be fairly straightforward in the future to modify the code to select the method based on each pair's distance apart. Additionally, a third integral method for calculating the coefficients (which converges faster) for near inclusions has not yet been implemented either.



## Usage

The main code is `tworotated.py`, which imports the necessary functions from the other python files. 

At present, these other files are:

- `setup.py` - funtions that specify the physical constants and far-field stress; set up coordinate systems; and visualise the starting geomery
- `etamu.py` - functions to calculate the expansion coefficients
- `matrix.py` - functions to set up and solve the matrix
- `potentials.py` - functions to generate $\phi$ and $\psi$ potentials
- `stress.py` - functions to solve the stress fields
- `plot.py` - functions to plot the stresses
- `sif.py` - function to calculate the $K_I$ and $K_{II}$ stress intensity factors for the inclusions
- `ressstress.py` - functions to generate the residual stress  



### Setting up the problem

Most setup variables can be changed from within the main `tworotated.py` file. This includes the geometry of the inclusions and stress, as well as the accuracy:

- the number of inclusions `Ntot`
- the angle the far field stress is applied at `ang`
- the major and minor axis lengths of each ellipse `l1` and `l2`
- the positions of the centers of the ellipses `zcenter`
- the angles each ellipse is tilted at `theta`
- the power of basis functions `nmax` (see [Choosing n](#Choosing n))
- the accuracies `mmax`, `lmax`, `jmax` used in calculating expansion coefficients

and also includes other variables to specify setting up the coordinate systems or the way that the stresses are evaluated. When set to `True`,

- `Global` allows superposition of the entire system in global z coordinates
- `EllCoords` computes in the elliptical coordinate system (see [A note on coordinate systems and superposition](#A note on coordinate systems and superposition) for more details)
- `ResStress` adds a residual stress to the final fields
- `Exp` includes the positive exponential basis functions for accurate fields near inclusions (see [Choosing n](#Choosing n))

Other relevant setup variables can be found and modified in `setup.py`, and include:

- the magnitude of the far field stress (currently set to a uniform normalised stress in the y-direction, i.e. S22 = 1, S11 = S12 = 0)
- the physical constants for the matrix and the inclusions - poisson's ratio `nu`, shear modulus `G`, and kappa `k` (which determines if in plane strain or stress)



### Plotting options

Many plotting options are available, and can be accessed by commenting in/out the code as necessary. 

The main plots are contour plots, which are used to visualise the stresses (individual or superimposed, with or without residual stresses). The limits of these plots can be changed by changing the limits at the beginning of the code when creating the local coordinate systems. The range of stresses and the number of contours plotted can be changed in `colourlim` and `levels`.

Line plots are most useful for checking the validity of the method, e.g. when the inclusions are aligned along the same horizontal line, so that using the index `middle` to index the stress gives the stresses along that 1D line which can be compared to other methods. There is also the option in elliptical coordinates to plot stresses around the perimeter of the ellipse.

Lastly, the function `StartPicture` near the beginning of the code can be used to visualise the starting geometry of the inclusions relative to one another, if such a visualisation might be useful. 



### Other code

Some files that have not yet been mentioned:

- `oneincl.py` - works for one inclusion with limited features (e.g. no rotating the ellipse, only z-coordinate system)
- `twoinclusions.py` - works for unrotated ellipses with limited features (only z-coordinate system). Note that this file and `tworotated.py` only work for a minimum of two inclusions.



## Acknowledgements

- Daniele Dini
- Mahdieh Ebrahimi