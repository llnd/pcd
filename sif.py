"Finds the stress intensity factors at crack tips."

import numpy as np

def SIF(d,eith,G0,n,Amp,Bmp):
    """
    Find the stress intensity factors at each inclusion's tips.
    """
    k = []
    k_m = []
    for q in range(len(d)):
        kf = -2*G0*np.sqrt(np.pi/d[q]*eith[q])
        kq = kf * n[:-1] * (Amp.conjugate()[:,q] + Bmp[:,q])
        k_q = kf * ((-1)**(n[:-1]-1)) * n[:-1] * (Amp.conjugate()[:,q] + Bmp[:,q])
        k.append(np.sum(kq))
        k_m.append(np.sum(k_q))
    k = np.asarray(k)
    k_m = np.asarray(k_m)

    # K_I and K_II
    k1 = k.real
    k2 = k.imag
    k1_m = k_m.real
    k2_m = k_m.imag

    return k1, k2, k1_m, k2_m
