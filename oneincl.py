#!/usr/bin/env python3

"Solving for the stress fields of a single ellipse (crack)."

import numpy as np
# from autograd import grad
import numpy.ma as ma
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.patches import Ellipse
import matplotlib.cm as cm
# from matplotlib.colors import LogNorm

def PhysConsts():
    """
    Defining physical constants:
    - matrix variables denoted (0), inclusion variables (1)

    nu : poisson's ratio
    G : shear modulus

    For cobalt, nu = 0.31, G = 75 GPa;
    For diamond, nu = 0.07, G = 478 GPa;
    For a crack or hole, nu = G = 0.

    kappa = 3-4*nu for plane strain, and 3-4*nu/1+nu for plane stress.
    """

    nu0 = 0.07
    nu1 = 0
    G0 = 570
    G1 = 0
    k0 = 3-4*nu0
    k1 = 3-4*nu1

    return nu0, nu1, G0, G1, k0, k1


def FarFieldStress(tilt):
    """
    Defining 2D stress state from imposed far field load.

    [can extend with theta later]
    """

    s11 = 0
    s22 = 1
    s12 = 0

    angle = tilt/180 * np.pi

    S11 = s11 * np.cos(angle)**2 - 2*s12*np.cos(angle)*np.sin(angle) + s22*np.sin(angle)**2
    S12 = s12 * np.cos(2*angle) + (s11 - s22)*np.cos(angle)*np.sin(angle)
    S22 = s22 * np.cos(angle)**2 + s11*np.sin(angle)**2 + s12*np.sin(2*angle)

    return S11, S22, S12


def RHS(k0, v0, ab, n):
    """
    Construct coefficients vector.

    k0 = kappa for matrix
    v0 = basis function
    ab = array [an, bn conjugate, a-n, b-n conjugate]

    rhs = RHS of the 4 equations to be solved for each n
    (note the 'b's are conjugated here)
    """

    v02 = v0**(2*n)
    coeffs = np.zeros((4,4))
    coeffs[0,0] = -k0
    coeffs[1,1] = coeffs[2,0] = coeffs[3,1] = -1
    coeffs[0,3] = v02
    coeffs[2,3] = coeffs[3,2] = -v02
    coeffs[1,2] = k0 * v02

    rhs = np.dot(coeffs, ab)

    return rhs


def Mcoeffs(k0, k1, v0, Z0, omega, n):
    """
    Define matrix (M1) for coefficients An, Bn, Cn, Dn;
    and their conjugates (M2).
    """

    v02 = v0**(2*n)
    M1 = np.zeros((4,4))
    M1[0,0] = k0
    M1[2,0] = 1
    M1[0,2] = -k1
    M1[1,2] = k1 * v02
    M1[2,2] = -omega
    M1[3,2] = -omega * v02

    M2 = np.zeros((4,4))
    M2[0,2] = -2 * n * v02 * np.sinh(2*Z0)
    M2[2,2] = 2 * n * omega * v02 * np.sinh(2*Z0)
    M2[0,3] = v02
    M2[1,1] = M2[3,1] = 1
    M2[1,3] = -1
    M2[2,3] = -omega * v02
    M2[3,3] = -omega

    return M1, M2

def SolveCoeffs(rhs, M1, M2):
    """
    Solve for An, Bn, Cn, Dn.
    """
    # Find real part
    rhs_real = rhs.real
    lhs_real = M1 + M2
    real = np.linalg.solve(lhs_real, rhs_real)

    # Find imaginary part
    rhs_im = rhs.imag
    lhs_im = M1 - M2
    imag = np.linalg.solve(lhs_im, rhs_im)

    coeffs = 1j*imag; coeffs += real

    return coeffs


def Potentials(ABCD, ab, xi, v0, Z0, n):
    """
    Generate complex potentials (currently no loops for
    non n=1 situations yet).
    """
    v = np.exp(xi)
    # phi
        # matrix
    phis = ABCD[0] * v**(-n)    # + n only
    phir = ab[0] * 2 * np.cosh(xi)  # +- n  (far field)
    phi0 = phis + phir
        # inclusion
    phi1 = ABCD[2] * 2 * np.cosh(xi)   # +- n

    # psi
        # matrix
            # local         + n only
    psis0 = ABCD[1] * v**(-n)
    psis1 = ( np.sinh(Z0) / np.sinh(xi) * (v/v0 - v0/v)
            * ABCD[0] * v**(-n))
    psis = psis0 - psis1
            # far field     +- n
    psirplus = ( ab[1] - 2*n*ab[0] * np.sinh(Z0) / np.sinh(xi)
                * np.sinh(xi - Z0) ) * v**(-n)
    psirminus = ( ab[3] - 2*n*ab[2] * np.sinh(Z0) / np.sinh(xi)
                * np.sinh(xi - Z0) ) * v**(n)
    psir = psirplus + psirminus
    psi0 = psis + psir
        # inclusion         +- n
    Dminus = ABCD[3] - 2 * n * np.sinh(2*Z0) * ABCD[2]
    psi10 = ABCD[3] * v**(-n) + Dminus * v**(n)
    psi11 = ( np.sinh(Z0) / np.sinh(xi) * (v/v0 - v0/v)
            * ABCD[2] * n * (v**(-n) - v**(n)))
    psi1 = psi10 - psi11

    return phi0, phi1, psi0, psi1

def dz(potential, stepsize):
    """
    Calculate the derivative wrt z where z = x+iy.
    """
    dy, dx = np.gradient(potential,stepsize)
    dz = dx - 1j*dy
    return dz

def displacements(k, wprime, phi, dphi, z):
    """
    Calculate displacements from potentials, in xy coordinates.
    Note in elliptical coordinates the expression for u below is multiplied
    by a prefactor (w'.conj/|w'|), but the same factor converts to xy coordinates
    so it cancels out.
    """
    u = k * phi - (z-z.conjugate())* dphi.conjugate() - phi.conjugate()
    ux = u.real
    uy = u.imag
    return ux, uy

def tractions(wprime, G, dphi, ddphi, dpsi, z):
    """
    Calculate tractions (sigma(zz) - i*sigma(ze)).
    """
    prefactor2 = wprime / wprime.conjugate()
    t = 2 * G * (dphi + dphi.conjugate() - prefactor2 * ((z.conjugate() - z)
        * ddphi - dphi + dpsi))
    return t

def stresses(G, dphi, ddphi, dpsi, z):
    """
    Calculate stresses in xy coordinates with the derivatives of
    potentials wrt z.
    """
    sum = 4 * G * (dphi + dphi.conjugate()).real
    rhs = 4 * G * ((z.conjugate()-z) * ddphi - dphi + dpsi)
    diff = rhs.real
    sigma12 = 0.5 * rhs.imag
    sigma22 = (0.5 * (sum+diff))
    sigma11 = (0.5 * (sum-diff))
    return sigma11, sigma22, sigma12

def plotq(X, Y, ux, uy, q):
    """
    Make a quiver plot of displacements, every qth point.
    """
    colors = np.arctan2(uy[::q,::q],ux[::q,::q])
    plt.figure(figsize=(8,8))
    plt.quiver(X[::q,::q], Y[::q,::q], ux[::q,::q], uy[::q,::q], colors,cmap='seismic', pivot='tail')
    plt.show()


def plotc(X, Y, function, name, levels, within, l1, l2):
    """
    Make a contour plot of stresses, at specified contours, without edges.
    """
    # Mask stresses within ellipse (and fill with zero bc hole for now)
    stress0 = ma.masked_array(function, mask=within)
    fn = stress0.filled(fill_value=0)

    fig, ax = plt.subplots(figsize=(6,6))
    # plt.figure(figsize=(6,6))
    contour = plt.contourf(X[1:-1,1:-1],Y[1:-1,1:-1],fn[1:-1,1:-1], levels, cmap=cm.coolwarm)
    plt.contour(X[1:-1,1:-1],Y[1:-1,1:-1],fn[1:-1,1:-1], levels, cmap=cm.coolwarm)
    plt.axis('equal')
    plt.colorbar(contour,fraction=0.046, pad=0.04)
    plt.title("{}".format(name))
    # Add ellipse
    ell = Ellipse((0,0),2*l1,2*l2, linewidth=0, fill=True, facecolor='w')
    ax.add_patch(ell)
    plt.show()



def main():
    # Define physical constants
    nu0, nu1, G0, G1, k0, k1 = PhysConsts()

    # Determine geometry of ellipse
    l1 = 1          # major axis length
    l2 = 0.4     # minor axis length
    e = l2/l1       # aspect ratio
    d = np.sqrt(l1*l1 - l2*l2)

    # Set up coefficients
    omega = G1/G0
    Z0 = 0.5 * np.log((1+e)/(1-e))      # eqn (2) of kusch
    v0 = np.exp(Z0)
    n = 1
    # nmax = 10

    # Define range
    spacing = 400
    limit = 4
    x = np.linspace(-limit,limit,spacing)
    y = np.linspace(-limit,limit,spacing)
    X, Y = np.meshgrid(x, y)
    def z(x,y):
        return x + 1j * y
    z = z(X, Y)
    xi = np.arccosh(z/d)

    # Define far field stress state
    tilt = 0                           # angle of applied stress (degrees)
    S11, S22, S12 = FarFieldStress(tilt)

    # Define a and b for n = +/-1
    aminus1 = d * (S11 + S22) / (16 * G0)
    bminus1 = aminus1 * v0**(-2) + d * complex(S22-S11, 2*S12) / (8 * G0)
    aplus1 = aminus1
    bplus1 = bminus1 + 2 * np.sinh(2*Z0) * aminus1
    print(v0**(-2))

    # Compute RHS of linear equations for n = 1
    ab_conj = np.array([aplus1,bplus1.conjugate(),aminus1,bminus1.conjugate()])
    rhs = RHS(k0, v0, ab_conj, n)

    # Arrange matrices of coefficients for LHS
    M1, M2 = Mcoeffs(k0, k1, v0, Z0, omega, n)

    # Solve for An, Bn, Cn, Dn
    ABCD = SolveCoeffs(rhs, M1, M2)
    print("ABCD = ", ABCD)

    # Find complex potentials
    ab = np.array([aplus1,bplus1,aminus1,bminus1])
    print("ab = a+1, b+1, a-1, b-1 = ", ab)
    phi0, phi1, psi0, psi1 = Potentials(ABCD, ab, xi, v0, Z0, n)

    # Find differentials of potentials (wrt z)
    wprime = d * np.sinh(xi)
    h = 2*2*limit/(spacing)
    dphi0dz = dz(phi0, h)
    dphi1dz = dz(phi1, h)
    d2phi0dz2 = dz(dphi0dz, h)
    d2phi1dz2 = dz(dphi1dz, h)
    dpsi0dz = dz(psi0, h)
    dpsi1dz = dz(psi1, h)

    # Calcuate displacements in xy coordinates
    # ux0, uy0 = displacements(k0, wprime, phi0, dphi0dz, z)
    # ux1, uy1 = displacements(k1, wprime, phi1, dphi1dz, z)

    # Plot displacements
    # plotq(X, Y, ux0, uy0, 15)

    # # Calculate tractions in elliptical coordinates
    # t0 = tractions(wprime, G0, dphi0dz, d2phi0dz2, dpsi0dz, z)
    # t1 = tractions(wprime, G1, dphi1dz, d2phi1dz2, dpsi1dz, z)

    # Calculate stresses in xy coordinates
    sig11_0, sig22_0, sig12_0 = stresses(G0, dphi0dz, d2phi0dz2, dpsi0dz, z)
    sig11_1, sig22_1, sig12_1 = stresses(G1, dphi1dz, d2phi1dz2, dpsi1dz, z)

    # Create mask for stresses
    def j(x,y):
        return (x/(d * np.cosh(Z0)))**2 + (y/(d * np.sinh(Z0)))**2
    j = j(X,Y)
    within = j <= 1
    # print(np.count_nonzero(within))

    # Plot stresses
    # levels = np.linspace(-0.5,0.5,11)
    plotc(X,Y,sig12_0, "sigma12", np.linspace(-0.5,0.5,11), within, l1, l2)
    plotc(X,Y,sig11_0, "sigma11", np.linspace(-1.0,0.8,19), within, l1, l2)
    plotc(X,Y,sig22_0, "sigma22", np.linspace(-1.5,1.5,31), within, l1, l2)

    # print(z)






    # fig = plt.figure(figsize=(8,8))
    # ax = fig.gca(projection='3d')
    # surf = ax.plot_surface(X, Y, checkphi1prime.real)
    # plt.show()
    # plt.savefig('sigma11.png')

    # plt.figure()
    # plt.contour(X, Y, checkphi1prime.real)
    # plt.colorbar()
    # plt.show()


    # # check that phi1' is consistent w analytical solution
    # # print(dphi1dz.real[0][::30])
    # def checkphi1prime(xi,ABCD, wprime):
    #     return 2 * ABCD[2] * np.sinh(xi) / wprime
    # checkphi1prime = checkphi1prime(xi, ABCD, wprime)
    # divide = checkphi1prime.real[0] / dphi1dz.real[0]
    # # print(checkphi1prime.real[0][::30])
    # print(divide[::30])
    #
    # # # check that phi1 is consistent with analytical solution
    # # def checkphi1(xi,ABCD, wprime):
    # #     return 2 * ABCD[2] * np.cosh(xi)
    # # checkphi1 = checkphi1(xi, ABCD, wprime)
    # # print(checkphi1[0][::10])
    # # print(phi1[0][::10])


if __name__ == '__main__':
    main()
